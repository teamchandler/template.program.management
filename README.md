# README #

Follow the steps to get your application up and running.

### What is this repository for? ###

This is a program management boilerplate
Ver. 1.0.0

### How do I get set up? ###

 * Clone the Repo to local
 * Bower install
 * follow the steps mentioned in this file for custom branding : https://docs.google.com/spreadsheets/d/1u5iNqu0fY6SwzZunku_RrVP5hSHxoubL_FtrAGZTlE0/edit#gid=0 
 * Database config changes in app/branding/config.js
 * To run the project open in webstorm
 * Deployment instructions - contact Isto Da.
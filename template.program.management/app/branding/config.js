
//var company ='schneider';
//var default_title ='Schneider Claim Portal';

//var prog_start_dt ='06-01-2014';
//var prog_end_dt ='12-31-2014';
//var to_email="info@seep-digilink.in";

//var report_link="http://qa.annectos.net:8080/index.php/";
//var api="http://localhost:20080";
//var master_api = "http://localhost:20080/master";
//var login_api = "http://qa.schneider2.annectos.net:20013";
//var api = "http://qa.schneider2.annectos.net:20013/master";

var login_api = "http://annectos.net:32015";
var api = "http://annectos.net:32015/master";
//var reward_api = "http://app.annectos.net/ecomm.schneider2_0.api";

//var reward_api = "http://localhost:8185";
//var login_api = "http://localhost:20015";
//var api = "http://localhost:20015/master";
//var master_api = "http://localhost:20050/master";


var upload_mode = "onbehalf";
var company ='vst';
var default_title ='Schneider Claim Portal';

var prog_start_dt ='06-01-2014';
var prog_end_dt ='12-31-2014';

//var to_email="info@seep-digilink.in";
//var to_email="dipanjan.dutta@oumtech.com";

//var report_link="http://qa.annectos.net:8080/index.php/loginService/authenticateLogger/";

var annectos_program_admin  =  ["annectos_admin@annectos.in","seema_program_admin@annectos.in","naveen_program_admin@annectos.in", "chandler@annectos.in"];
var schneider_program_admin = ["schneider_program_admin@annectos.in", "chandler@annectos.in"];
var rsm_admin = ["rohit.pohurkar@schneider-electric.com","fazal.tabraze@schneider-electric.com","shailesh.kumar@schneider-electric.com","Arindam.Roy@schneider-electric.com"];

var encryption_phrase = "June2014";

var state_list = [
    { id: "AP", text: "Andhra Pradesh" },
    { id: "AR", text: "Arunachal Pradesh" },
    { id: "AS", text: "Assam" },
    { id: "BR", text: "Bihar" },
    { id: "CH", text: "Chhattisgarh" },
    { id: "DL", text: "Delhi" },
    { id: "GA", text: "Goa" },
    { id: "GJ", text: "Gujarat" },
    { id: "HR", text: "Haryana" },
    { id: "HP", text: "Himachal Pradesh" },
    { id: "JK", text: "Jammu and Kashmir" },
    { id: "KA", text: "Karnataka" },
    { id: "KL", text: "Kerala" },
    { id: "MP", text: "Madhya Pradesh" },
    { id: "MH", text: "Maharashtra" },
    { id: "MN", text: "Manipur" },
    { id: "ML", text: "Meghalaya" },
    { id: "MZ", text: "Mizoram" },
    { id: "NL", text: "Nagaland" },
    { id: "OR", text: "Orissa" },
    { id: "Pb", text: "Punjab" },
    { id: "UP", text: "Uttar Preadesh" },
    { id: "UT", text: "Uttaranchal" },
    { id: "RJ", text: "Rajasthan" },
    { id: "SK", text: "Sikkim" },
    { id: "TN", text: "Tamil Nadu" },
    { id: "TR", text: "Tripura" },
    { id: "WB", text: "West Bengal" }];


 var program_rules =
     [
         {
             "program_code": "nrml",
             "rules": [
                 {
                     "min_val": 500000,
                     "max_val": 999999,
                     "award": 500
                 },
                 {
                     "min_val": 1000000,
                     "max_val": 1499999,
                     "award": 1000
                 },
                 {
                     "min_val": 1500000,
                     "max_val": 1999999,
                     "award": 1500
                 },
                 {
                     "min_val": 2000000,
                     "max_val": 2499999,
                     "award": 2000
                 },
                 {
                     "min_val": 2500000,
                     "max_val": 2999999,
                     "award": 2500
                 },
                 {
                     "min_val": 3000000,
                     "max_val": 3499999,
                     "award": 3000
                 },
                 {
                     "min_val": 3500000,
                     "max_val": 3999999,
                     "award": 3500
                 },
                 {
                     "min_val": 4000000,
                     "max_val": 4499999,
                     "award": 4000
                 },
                 {   "min_val": 4500000, "max_val": 4999999, "award": 4500 },
                 {   "min_val": 5000000, "max_val": 5499999, "award": 5000 },
                 {   "min_val": 5500000, "max_val": 5999999, "award": 5500 },
                 {   "min_val": 6000000, "max_val": 6499999, "award": 6000 },
                 {   "min_val": 6500000, "max_val": 6999999, "award": 6500 },
                 {   "min_val": 7000000, "max_val": 7499999, "award": 7000 },
                 {   "min_val": 7500000, "max_val": 7999999, "award": 7500 },
                 {   "min_val": 8000000, "max_val": 8499999, "award": 8000 },
                 {   "min_val": 8500000, "max_val": 8999999, "award": 8500 },
                 {   "min_val": 9000000, "max_val": 9499999, "award": 9000 },
                 {   "min_val": 9500000, "max_val": 9999999, "award": 9500 }
             ]
         },
         {
             "program_code": "spl1",
             "rules": [

                 { "min_val": 100000,  "max_val": 199999,  "award": 50   },
                 { "min_val": 200000,  "max_val": 299999,  "award": 100  },
                 { "min_val": 300000,  "max_val": 399999,  "award": 150  },
                 { "min_val": 400000,  "max_val": 499999,  "award": 200  },
                 { "min_val": 500000,  "max_val": 599999,  "award": 250  },
                 { "min_val": 600000,  "max_val": 699999,  "award": 300  },
                 { "min_val": 700000,  "max_val": 799999,  "award": 350  },
                 { "min_val": 800000,  "max_val": 899999,  "award": 400  },
                 { "min_val": 900000,  "max_val": 999999,  "award": 450  },
                 { "min_val": 1000000, "max_val": 1099999, "award": 500  },
                 { "min_val": 1100000, "max_val": 1199999, "award": 550  },
                 { "min_val": 1200000, "max_val": 1299999, "award": 600  },
                 { "min_val": 1300000, "max_val": 1399999, "award": 650  },
                 { "min_val": 1400000, "max_val": 1499999, "award": 700  },
                 { "min_val": 1500000, "max_val": 1599999, "award": 750  },
                 { "min_val": 1600000, "max_val": 1699999, "award": 800  },
                 { "min_val": 1700000, "max_val": 1799999, "award": 850  },
                 { "min_val": 1800000, "max_val": 1899999, "award": 900  },
                 { "min_val": 1900000, "max_val": 1999999, "award": 950  },
                 { "min_val": 2000000, "max_val": 2099999, "award": 1000 },
                 { "min_val": 2100000, "max_val": 2199999, "award": 1050 },
                 { "min_val": 2200000, "max_val": 2299999, "award": 1100 },
                 { "min_val": 2300000, "max_val": 2399999, "award": 1150 },
                 { "min_val": 2400000, "max_val": 2499999, "award": 1200 },
                 { "min_val": 2500000, "max_val": 2599999, "award": 1250 },
                 { "min_val": 2600000, "max_val": 2699999, "award": 1300 },
                 { "min_val": 2700000, "max_val": 2799999, "award": 1350 },
                 { "min_val": 2800000, "max_val": 2899999, "award": 1400 },
                 { "min_val": 2900000, "max_val": 2999999, "award": 1450 },
                 { "min_val": 3000000, "max_val": 3099999, "award": 1500 },
                 { "min_val": 3100000, "max_val": 3199999, "award": 1550 },
                 { "min_val": 3200000, "max_val": 3299999, "award": 1600 },
                 { "min_val": 3300000, "max_val": 3399999, "award": 1650 },
                 { "min_val": 3400000, "max_val": 3499999, "award": 1700 },
                 { "min_val": 3500000, "max_val": 3599999, "award": 1750 },
                 { "min_val": 3600000, "max_val": 3699999, "award": 1800 },
                 { "min_val": 3700000, "max_val": 3799999, "award": 1850 },
                 { "min_val": 3800000, "max_val": 3899999, "award": 1900 },
                 { "min_val": 3900000, "max_val": 3999999, "award": 1950 },
                 { "min_val": 4000000, "max_val": 4099999, "award": 2000 },
                 { "min_val": 4100000, "max_val": 4199999, "award": 2050 },
                 { "min_val": 4200000, "max_val": 4299999, "award": 2100 },
                 { "min_val": 4300000, "max_val": 4399999, "award": 2150 },
                 { "min_val": 4400000, "max_val": 4499999, "award": 2200 },
                 { "min_val": 4500000, "max_val": 4599999, "award": 2250 },
                 { "min_val": 4600000, "max_val": 4699999, "award": 2300 },
                 { "min_val": 4700000, "max_val": 4799999, "award": 2350 },
                 { "min_val": 4800000, "max_val": 4899999, "award": 2400 },
                 { "min_val": 4900000, "max_val": 4999999, "award": 2450 },
                 { "min_val": 5000000, "max_val": 5099999, "award": 2500 },
                 { "min_val": 5100000, "max_val": 5199999, "award": 2550 },
                 { "min_val": 5200000, "max_val": 5299999, "award": 2600 },
                 { "min_val": 5300000, "max_val": 5399999, "award": 2650 },
                 { "min_val": 5400000, "max_val": 5499999, "award": 2700 },
                 { "min_val": 5500000, "max_val": 5599999, "award": 2750 },
                 { "min_val": 5600000, "max_val": 5699999, "award": 2800 },
                 { "min_val": 5700000, "max_val": 5799999, "award": 2850 },
                 { "min_val": 5800000, "max_val": 5899999, "award": 2900 },
                 { "min_val": 5900000, "max_val": 5999999, "award": 2950 },
                 { "min_val": 6000000, "max_val": 6099999, "award": 3000 },
                 { "min_val": 6100000, "max_val": 6199999, "award": 3050 },
                 { "min_val": 6200000, "max_val": 6299999, "award": 3100 },
                 { "min_val": 6300000, "max_val": 6399999, "award": 3150 },
                 { "min_val": 6400000, "max_val": 6499999, "award": 3200 },
                 { "min_val": 6500000, "max_val": 6599999, "award": 3250 },
                 { "min_val": 6600000, "max_val": 6699999, "award": 3300 },
                 { "min_val": 6700000, "max_val": 6799999, "award": 3350 },
                 { "min_val": 6800000, "max_val": 6899999, "award": 3400 },
                 { "min_val": 6900000, "max_val": 6999999, "award": 3450 },
                 { "min_val": 7000000, "max_val": 7099999, "award": 3500 },
                 { "min_val": 7100000, "max_val": 7199999, "award": 3550 },
                 { "min_val": 7200000, "max_val": 7299999, "award": 3600 },
                 { "min_val": 7300000, "max_val": 7399999, "award": 3650 },
                 { "min_val": 7400000, "max_val": 7499999, "award": 3700 },
                 { "min_val": 7500000, "max_val": 7599999, "award": 3750 },
                 { "min_val": 7600000, "max_val": 7699999, "award": 3800 },
                 { "min_val": 7700000, "max_val": 7799999, "award": 3850 },
                 { "min_val": 7800000, "max_val": 7899999, "award": 3900 },
                 { "min_val": 7900000, "max_val": 7999999, "award": 3950 },
                 { "min_val": 8000000, "max_val": 8099999, "award": 4000 },
                 { "min_val": 8100000, "max_val": 8199999, "award": 4050 },
                 { "min_val": 8200000, "max_val": 8299999, "award": 4100 },
                 { "min_val": 8300000, "max_val": 8399999, "award": 4150 },
                 { "min_val": 8400000, "max_val": 8499999, "award": 4200 },
                 { "min_val": 8500000, "max_val": 8599999, "award": 4250 },
                 { "min_val": 8600000, "max_val": 8699999, "award": 4300 },
                 { "min_val": 8700000, "max_val": 8799999, "award": 4350 },
                 { "min_val": 8800000, "max_val": 8899999, "award": 4400 },
                 { "min_val": 8900000, "max_val": 8999999, "award": 4450 },
                 { "min_val": 9000000, "max_val": 9099999, "award": 4500 },
                 { "min_val": 9100000, "max_val": 9199999, "award": 4550 },
                 { "min_val": 9200000, "max_val": 9299999, "award": 4600 },
                 { "min_val": 9300000, "max_val": 9399999, "award": 4650 },
                 { "min_val": 9400000, "max_val": 9499999, "award": 4700 },
                 { "min_val": 9500000, "max_val": 9599999, "award": 4750 },
                 { "min_val": 9600000, "max_val": 9699999, "award": 4800 },
                 { "min_val": 9700000, "max_val": 9799999, "award": 4850 },
                 { "min_val": 9800000, "max_val": 9899999, "award": 4900 },
                 { "min_val": 9900000, "max_val": 9999999, "award": 4950 }

             ]
         },
         {
             "program_code": "mixed",
             "rules": [

                 { "min_val": 500,   "max_val": 999,   "award": 500   },
                 { "min_val": 1000,  "max_val": 1499,  "award": 1000  },
                 { "min_val": 1500,  "max_val": 1999,  "award": 1500  },
                 { "min_val": 2000,  "max_val": 2499,  "award": 2000  },
                 { "min_val": 2500,  "max_val": 2999,  "award": 2500  },
                 { "min_val": 3000,  "max_val": 3499,  "award": 3000  },
                 { "min_val": 3500,  "max_val": 3999,  "award": 3500  },
                 { "min_val": 4000,  "max_val": 4499,  "award": 4000  },
                 { "min_val": 4500,  "max_val": 4999,  "award": 4500  },
                 { "min_val": 5000,  "max_val": 5499,  "award": 5000  },
                 { "min_val": 5500,  "max_val": 5999,  "award": 5500  },
                 { "min_val": 6000,  "max_val": 6499,  "award": 6000  },
                 { "min_val": 6500,  "max_val": 6999,  "award": 6500  },
                 { "min_val": 7000,  "max_val": 7499,  "award": 7000  },
                 { "min_val": 7500,  "max_val": 7999,  "award": 7500  },
                 { "min_val": 8000,  "max_val": 8499,  "award": 8000  },
                 { "min_val": 8500,  "max_val": 8999,  "award": 8500  },
                 { "min_val": 9000,  "max_val": 9499,  "award": 9000  },
                 { "min_val": 9500,  "max_val": 9999,  "award": 9500  },
                 { "min_val": 10000, "max_val": 10499, "award": 10000 },
                 { "min_val": 10500, "max_val": 10999, "award": 10500 },
                 { "min_val": 11000, "max_val": 11499, "award": 11000 },
                 { "min_val": 11500, "max_val": 11999, "award": 11500 },
                 { "min_val": 12000, "max_val": 12499, "award": 12000 },
                 { "min_val": 12500, "max_val": 12999, "award": 12500 },
                 { "min_val": 13000, "max_val": 13499, "award": 13000 },
                 { "min_val": 13500, "max_val": 13999, "award": 13500 },
                 { "min_val": 14000, "max_val": 14499, "award": 14000 },
                 { "min_val": 14500, "max_val": 14999, "award": 14500 },
                 { "min_val": 15000, "max_val": 15499, "award": 15000 },
                 { "min_val": 15500, "max_val": 15999, "award": 15500 },
                 { "min_val": 16000, "max_val": 16499, "award": 16000 },
                 { "min_val": 16500, "max_val": 16999, "award": 16500 },
                 { "min_val": 17000, "max_val": 17499, "award": 17000 },
                 { "min_val": 17500, "max_val": 17999, "award": 17500 },
                 { "min_val": 18000, "max_val": 18499, "award": 18000 },
                 { "min_val": 18500, "max_val": 18999, "award": 18500 },
                 { "min_val": 19000, "max_val": 19499, "award": 19000 },
                 { "min_val": 19500, "max_val": 19999, "award": 19500 },
                 { "min_val": 20000, "max_val": 20499, "award": 20000 },
                 { "min_val": 20500, "max_val": 20999, "award": 20500 },
                 { "min_val": 21000, "max_val": 21499, "award": 21000 },
                 { "min_val": 21500, "max_val": 21999, "award": 21500 },
                 { "min_val": 22000, "max_val": 22499, "award": 22000 },
                 { "min_val": 22500, "max_val": 22999, "award": 22500 },
                 { "min_val": 23000, "max_val": 23499, "award": 23000 },
                 { "min_val": 23500, "max_val": 23999, "award": 23500 },
                 { "min_val": 24000, "max_val": 24499, "award": 24000 },
                 { "min_val": 24500, "max_val": 24999, "award": 24500 },
                 { "min_val": 25000, "max_val": 25499, "award": 25000 },
                 { "min_val": 25500, "max_val": 25999, "award": 25500 },
                 { "min_val": 26000, "max_val": 26499, "award": 26000 },
                 { "min_val": 26500, "max_val": 26999, "award": 26500 },
                 { "min_val": 27000, "max_val": 27499, "award": 27000 },
                 { "min_val": 27500, "max_val": 27999, "award": 27500 },
                 { "min_val": 28000, "max_val": 28499, "award": 28000 },
                 { "min_val": 28500, "max_val": 28999, "award": 28500 },
                 { "min_val": 29000, "max_val": 29499, "award": 29000 },
                 { "min_val": 29500, "max_val": 29999, "award": 29500 },
                 { "min_val": 30000, "max_val": 30499, "award": 30000 },
                 { "min_val": 30500, "max_val": 30999, "award": 30500 },
                 { "min_val": 31000, "max_val": 31499, "award": 31000 },
                 { "min_val": 31500, "max_val": 31999, "award": 31500 },
                 { "min_val": 32000, "max_val": 32499, "award": 32000 },
                 { "min_val": 32500, "max_val": 32999, "award": 32500 },
                 { "min_val": 33000, "max_val": 33499, "award": 33000 },
                 { "min_val": 33500, "max_val": 33999, "award": 33500 },
                 { "min_val": 34000, "max_val": 34499, "award": 34000 },
                 { "min_val": 34500, "max_val": 34999, "award": 34500 },
                 { "min_val": 35000, "max_val": 35499, "award": 35000 },
                 { "min_val": 35500, "max_val": 35999, "award": 35500 },
                 { "min_val": 36000, "max_val": 36499, "award": 36000 },
                 { "min_val": 36500, "max_val": 36999, "award": 36500 },
                 { "min_val": 37000, "max_val": 37499, "award": 37000 },
                 { "min_val": 37500, "max_val": 37999, "award": 37500 },
                 { "min_val": 38000, "max_val": 38499, "award": 38000 },
                 { "min_val": 38500, "max_val": 38999, "award": 38500 },
                 { "min_val": 39000, "max_val": 39499, "award": 39000 },
                 { "min_val": 39500, "max_val": 39999, "award": 39500 },
                 { "min_val": 40000, "max_val": 40499, "award": 40000 },
                 { "min_val": 40500, "max_val": 40999, "award": 40500 },
                 { "min_val": 41000, "max_val": 41499, "award": 41000 },
                 { "min_val": 41500, "max_val": 41999, "award": 41500 },
                 { "min_val": 42000, "max_val": 42499, "award": 42000 },
                 { "min_val": 42500, "max_val": 42999, "award": 42500 },
                 { "min_val": 43000, "max_val": 43499, "award": 43000 },
                 { "min_val": 43500, "max_val": 43999, "award": 43500 },
                 { "min_val": 44000, "max_val": 44499, "award": 44000 },
                 { "min_val": 44500, "max_val": 44999, "award": 44500 },
                 { "min_val": 45000, "max_val": 45499, "award": 45000 },
                 { "min_val": 45500, "max_val": 45999, "award": 45500 },
                 { "min_val": 46000, "max_val": 46499, "award": 46000 },
                 { "min_val": 46500, "max_val": 46999, "award": 46500 },
                 { "min_val": 47000, "max_val": 47499, "award": 47000 },
                 { "min_val": 47500, "max_val": 47999, "award": 47500 },
                 { "min_val": 48000, "max_val": 48499, "award": 48000 },
                 { "min_val": 48500, "max_val": 48999, "award": 48500 },
                 { "min_val": 49000, "max_val": 49499, "award": 49000 },
                 { "min_val": 49500, "max_val": 49999, "award": 49500 }


             ]
         }


     ]



var user_type = "schneider user";

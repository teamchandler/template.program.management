/**
 * Created by DRIPL_1 on 04/17/2015.
 */

var menu = [
    {'display_name': 'Home', 'link': '#/main/home', 'state': 'main.home', 'admin': 0},
    {'display_name': 'PROFILE', 'link': '#/main/profile', 'state': 'main.profile', 'admin': 0},
    {'display_name': 'Program', 'link': '#/main/program', 'state': 'main.program', 'admin': 0},
    {'display_name': 'Claim Entry', 'link': '#/main/claim', 'state': 'main.claim', 'admin': 0},
    {'display_name': 'My Claim', 'link': '#/main/my_claim', 'state': 'main.my_claim', 'admin': 0},
    {'display_name': 'Redemption', 'link': '#/main/redemption', 'state': 'main.redemption', 'admin': 0},
    {'display_name': 'Reward Gallery', 'link': '/rewards', 'state': '', 'admin': 0},
    {'display_name': 'Contact', 'link': '#/main/contact', 'state': 'main.contact', 'admin': 0},
    {'display_name': 'Prog Adm', 'link': '#/main/prog_admin', 'state': 'main.prog_admin', 'admin': 1},
    {'display_name': 'Special', 'link': '#/main/special/ids', 'state': 'main.special', 'admin': 0},
    {'display_name': 'Report', 'link': '', 'state': '', 'admin': 1},
    {'display_name': 'Change Password', 'link': '#/main/changepass', 'state': 'main.changepass', 'admin': 0},
//    {'display_name': 'Dash', 'link': '#/main/dash', 'state': 'main.dash', 'admin': 1},
    {'display_name': 'Logout', 'link': '#/login/login#login_box', 'state': 'login.login', 'admin': 0}
];

var default_menu = "main.home";

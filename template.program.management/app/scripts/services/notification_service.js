'use strict';

angular.module('vstford')

    .service('NotificationService', function NotificationService($rootScope) {

// ***************************************************************
//          Data Elements that will be shared across
// ***************************************************************
//        var new_product_list = "";
//        var min_price = 0;
//        var max_price = 0;
// ***************************************************************

// ***************************************************************
//          This is to centralize all notifcations used
//          in all the controllers
// ***************************************************************
        // This is used for sending broadcast message and data from rasing controller


        this.error_occured = function (broadcast_message, error_object, source, o) {
            this.error_object  = error_object;
            this.error_source = source;
            this.o = o;
            this.notify(broadcast_message)
        };
        this.change_header = function (broadcast_message, header_label) {
            this.header_label =  header_label;
            this.notify("change header")
        };
        this.send_notification = function (broadcast_message, notification_data) {
            this.notification_data =  notification_data;
            this.notify(broadcast_message)
        };

//********** Generic Notification ********************
        this.notify = function(broadcast_message) {
            $rootScope.$broadcast(broadcast_message);
        };
//*****************************************************

        //Show loader
        this.show_loader = function (broadcast_message, loader_condition) {
            this.loader_condition = loader_condition;
            this.notify("show loader")
        };

        //Hide loader
        this.hide_loader = function (broadcast_message, loader_condition) {
            this.loader_condition = loader_condition;
            this.notify("hide loader")
        };


    });

/**
 * Created by Hiru on 3/11/2015.
 */

'use strict';


angular.module('vstford')
    .service('LoginService', function MasterService(UtilService) {

        this.user_login = function($http,$q,user){

            var apiPath =  UtilService.get_api() + '/login/by';

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data:user,
                ContentType: 'application/json'
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;

        }

        this.user_terms_condition_update = function($http,$q,terms){

            var apiPath =  UtilService.get_master_api() + '/user/terms/condition/update';
            var deferred = $q.defer();
            $http({
                method: 'POST',
                headers: {token: UtilService.get_from_localstorage('token')},
                url: apiPath,
                data:terms,
                ContentType: 'application/json'
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;

        }

        this.change_password = function($http,$q,credential){

            var apiPath =  UtilService.get_master_api() +  '/user/change/password';
            var deferred = $q.defer();
            $http({
                method: 'POST',
                headers: {token: UtilService.get_from_localstorage('token')},
                url: apiPath,
                data:credential,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);

            }).error(function (data)
            {
                deferred.reject("Data Error");


            })
            return deferred.promise;


        }


        this.ResetUserPassword = function ($http, $q, dataobj) {

            //   var apiPath = UtilService.get_reward_api()  + '/user/registration/ResetPassword/';
            var apiPath = UtilService.get_reward_api() + '/user/registration/ResetPassword/';
            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: apiPath,
                data: dataobj,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })
            return deferred.promise;

        };

        this.SaveNewPassword = function ($http, $q, dataobj) {
            var apiPath = UtilService.get_reward_api() + '/user/registration/SaveNewPassword/';
            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: apiPath,
                data: dataobj,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);

                //localStorageService.add('user_info', data[0]);
            }).error(function (data)
            {
                deferred.reject("An error occured while validating User");
            })
            return deferred.promise;
        };

        this.resetPwdCheck = function ($http, $q, dataobj) {
            var apiPath = UtilService.get_reward_api() + '/user/registration/checkEmailGuid/';
            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: apiPath,
                data: dataobj,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
                //localStorageService.add('user_info', data[0]);
            }).error(function (data)
            {
                deferred.reject("An error occured while validating User");
            })
            return deferred.promise;
        };




    });


/**
 * Created by developer6 on 6/12/2014.
 */
'use strict';

angular.module('vstford')
  .service('ClaimService', function Menu(UtilService) {

    this.get_retailer_list = function($http, $q) {

      var apiPath = UtilService.get_master_api() +
        '/retailer/list';
      var token = UtilService.get_from_localstorage('token');

      var deferred = $q.defer();
      $http({
        method: 'GET',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        type: JSON

      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    }


    this.get_user_company_name = function($http, $q, company, srch_key) {

      var apiPath = UtilService.get_master_api() +
        '/get/user/company/name/by/' + company + "/" + srch_key;

      var deferred = $q.defer();
      $http({
        method: 'GET',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        type: JSON
      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    }

    // New One

    this.get_user_company_name_new = function($http, $q, company, srch_key) {

      var apiPath = UtilService.get_master_api() +
        '/get/user/company/name/new/by/' + company + "/" + srch_key;

      var deferred = $q.defer();
      $http({
        method: 'GET',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        type: JSON
      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    }

    //




    this.save_invoice = function($http, $q, dataobj) {

      var apiPath = UtilService.get_master_api() +
        '/invoice/schneider/add';

      var deferred = $q.defer();
      $http({
        method: 'POST',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        data: dataobj,
        ContentType: 'application/json'

      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    }

    this.add_override_sku = function($http, $q, dataobj) {

      var apiPath = UtilService.get_master_api() +
        '/override/sku/add';

      var deferred = $q.defer();
      $http({
        method: 'POST',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        data: dataobj,
        ContentType: 'application/json'

      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    }


    this.get_all_company_name = function($http, $q) {
      var apiPath = UtilService.get_master_api() +
        '/user/company';

      var deferred = $q.defer();
      $http({
        method: 'GET',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        type: JSON
      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    }

    this.get_user_by_comp_name = function($http, $q, comp_name) {
      var apiPath = UtilService.get_master_api() +
        '/user/details/' + comp_name;

      var deferred = $q.defer();
      $http({
        method: 'GET',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        type: JSON
      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    }

    this.get_invoice_by_user_name = function($http, $q, user_name) {
      var apiPath = UtilService.get_master_api() +
        '/get/invoice/detail/by/user/' + user_name;

      var deferred = $q.defer();
      $http({
        method: 'GET',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        type: JSON
      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    }

    this.get_invoice_by_retailer_name = function($http, $q, retailer_name) {
      var apiPath = UtilService.get_master_api() +
        '/get/invoice/detail/by/retailer/' + retailer_name;

      var deferred = $q.defer();
      $http({
        method: 'GET',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        type: JSON
      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    }




    this.get_duplicate_invoice_list = function($http, $q, invoice_number) {

      var dataobj = {};
      dataobj.invoice_number = invoice_number;

      var apiPath = UtilService.get_master_api() +
        '/search/duplicate/invoice/list';

      var deferred = $q.defer();
      $http({
        method: 'POST',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        data: dataobj,
        ContentType: 'application/json'

      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    }



    this.get_invoce_by_status_comp_name = function($http, $q, status, company_name) {
      var apiPath = UtilService.get_master_api() +
        '/search/invoice/by/' + status + "/" + company_name;


      var deferred = $q.defer();
      $http({
        method: 'GET',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        type: JSON
      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    }



    this.search_by_sku = function($http, $q, sku) {
      var apiPath = UtilService.get_master_api() +
        '/search/by/sku/' + sku;

      var deferred = $q.defer();
      $http({
        method: 'GET',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        type: JSON
      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    }



    this.search_by_part_code = function($http, $q, sku) {

      var dataobj = {};
      dataobj.part_code = sku;

      var apiPath = UtilService.get_master_api() + '/search/by/part/code';

      var deferred = $q.defer();
      $http({
        method: 'POST',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        data: dataobj,
        ContentType: 'application/json'
      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    }


    this.search_sku_list = function($http, $q, sku) {
      var apiPath = UtilService.get_master_api() +
        '/search/sku/list/by/sku/' + sku;

      var deferred = $q.defer();
      $http({
        method: 'GET',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        type: JSON
      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    }

    /************Added By Dipanjan**************/

    this.invoice_details_by_number = function($http, $q, claim_id) {
      var apiPath = UtilService.get_master_api() +
        '/invoice/detail/by/id/' + claim_id;

      var deferred = $q.defer();
      $http({
        method: 'GET',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        type: JSON
      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    }

    this.save_sku = function($http, $q, dataobj) {

      var apiPath = UtilService.get_master_api() +
        '/sku/add';

      var deferred = $q.defer();
      $http({
        method: 'POST',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        data: dataobj,
        ContentType: 'application/json'

      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    }



    this.update_claim_details = function($http, $q, claim_id, inv_amount, claim_details, modified_by, modified_date, status, approval_comments, amount_track, inv_date_track, invoice_date, invoice_amount_excl_vat) {

      var dataobj = {};
      dataobj.claim_id = claim_id;
      dataobj.claim_details = claim_details;
      dataobj.status = status;
      dataobj.approval_comments = approval_comments;
      dataobj.invoice_amount = inv_amount;
      dataobj.amount_track = amount_track;
      dataobj.inv_date_track = inv_date_track;
      dataobj.invoice_date = invoice_date;
      dataobj.modified_by = modified_by;
      dataobj.modified_date = modified_date;
      dataobj.invoice_amount_excl_vat = invoice_amount_excl_vat;

      var apiPath = UtilService.get_master_api() +
        '/invoice/claim/details/update';

      var deferred = $q.defer();
      $http({
        method: 'POST',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        data: dataobj,
        ContentType: 'application/json'

      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    }

    this.trigger_email_srvc = function($http, $q, dataobj) {

      var apiPath = 'http://emailblustengin.annectos.net:3000/email';

      console.log(dataobj);
      var deferred = $q.defer();
      $http({
        method: 'POST',

        url: apiPath,
        data: dataobj,
        ContentType: 'application/json'

      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    }



    this.get_top_si = function($http, $q, claim_id) {
      var apiPath = UtilService.get_master_api() + '/top/si';

      var deferred = $q.defer();
      $http({
        method: 'GET',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        type: JSON
      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    }



    this.get_top_si_by_region = function($http, $q, claim_id) {
      var apiPath = UtilService.get_master_api() + '/top/si/by/region';

      var deferred = $q.defer();
      $http({
        method: 'GET',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        type: JSON
      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    }


    this.get_si_by_region = function($http, $q, claim_id) {
      var apiPath = UtilService.get_master_api() + '/si/by/region';

      var deferred = $q.defer();
      $http({
        method: 'GET',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        type: JSON
      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    }


    this.get_normal_si_qualifier = function($http, $q, claim_id) {
      var apiPath = UtilService.get_master_api() + '/normal/program/qualifier/si';

      var deferred = $q.defer();
      $http({
        method: 'GET',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        type: JSON
      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    }


    this.get_special_si_qualifier = function($http, $q, claim_id) {
      var apiPath = UtilService.get_master_api() + '/special/program/qualifier/si';

      var deferred = $q.defer();
      $http({
        method: 'GET',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        type: JSON
      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    }

    this.get_details_by_user = function($http, $q, user_id) {
      var apiPath = UtilService.get_master_api() + '/details/by/user/' + user_id;

      var deferred = $q.defer();
      $http({
        method: 'GET',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        type: JSON
      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    }

    /**
     * Called function from controller definition defined here
     * @params user_id passed from controller to this service
     * this defined as constructor to this service, constructing the function from respective controller
     * get_api() is path of api defined in utilservices service where the path/to/api/function/  is defined
     */
    this.get_details_total_sale_by_user = function($http, $q, user_id) {
      var apiPath = UtilService.get_master_api() + '/details/totalsale/by/user/' + user_id;

      var deferred = $q.defer();
      $http({
        method: 'GET',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        type: JSON
      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    }

    this.get_user_details_for_zone_address = function($http, $q, user_id) {
      var apiPath = UtilService.get_master_api() + '/details/zoneaddress/by/user/' + user_id;

      var deferred = $q.defer();
      $http({
        method: 'GET',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        type: JSON
      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error")
      })
      return deferred.promise;

    }

    this.posting_goal_value = function($http, $q, goal, user) {
      var dataobj = {};
      dataobj.goal = goal;
      dataobj.user = user;

      var apiPath = UtilService.get_master_api() + '/goal/set/by/user';
      var deferred = $q.defer();
      $http({
        method: 'POST',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        data: dataobj,
        type: JSON
      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error")
      })
      return deferred.promise;



    }

    this.get_user_goal_value = function($http, $q, user_id) {
      var apiPath = UtilService.get_master_api() + '/details/goal/value/of/user/' + user_id;

      var deferred = $q.defer();
      $http({
        method: 'GET',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        type: JSON
      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error")
      })
      return deferred.promise;

    }

    this.updating_goal_value = function($http, $q, goal, user_id) {
      var dataobj = {};
      dataobj.goal = goal;
      dataobj.user = user_id;

      var apiPath = UtilService.get_master_api() + '/goal/update/by/user';
      var deferred = $q.defer();
      $http({
        method: 'POST',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        data: dataobj,
        type: JSON
      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error")
      })
      return deferred.promise;
    }


    this.get_user_redeemed_points = function($http, $q, user_id) {
      var apiPath = UtilService.get_master_api() + '/redeemed/points/of/user/' + user_id;
      var deferred = $q.defer();
      $http({
        method: 'GET',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        type: JSON
      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error")
      })
      return deferred.promise;
    }

    this.get_user_proceedings_invoice_current_month = function($http, $q, user_id, current_date) {
      var apiPath = UtilService.get_master_api() + '/invoice/month/proceedings/of/user/' + user_id + '/' + current_date;
      var deferred = $q.defer();
      $http({
        method: 'GET',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        type: JSON
      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error")
      })
      return deferred.promise;
    }

    this.get_user_si_region_dashboard = function($http, $q, $regionName) {
      var apiPath = UtilService.get_master_api() + '/si/by/region/dashboard/' + $regionName;
      var deferred = $q.defer();
      $http({
        method: 'GET',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        type: JSON
      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error")
      })
      return deferred.promise;
    }

    this.get_user_si_allIndia_dashboard = function($http, $q) {
      var apiPath = UtilService.get_master_api() + '/si/by/allIndia/dashboard';
      var deferred = $q.defer();
      $http({
        method: 'GET',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        type: JSON
      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error")
      })
      return deferred.promise;
    }

    this.get_sku_list = function($http, $q) {

      var apiPath = UtilService.get_master_api() +
        '/sku/list/all';

      var deferred = $q.defer();
      $http({
        method: 'GET',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        type: JSON
      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    }


    this.get_sku_list_by_search_key = function($http, $q, sku_search_key) {

      var apiPath = UtilService.get_master_api() +
        '/sku/list/search/key';

      var deferred = $q.defer();
      $http({
        method: 'POST',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        data: { sku_search_key: sku_search_key },
        type: JSON
      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    }

    this.get_sku_by_name = function($http, $q, name) {
      var apiPath = UtilService.get_master_api() +
        '/get/sku/by/name/' + name;

      var deferred = $q.defer();
      $http({
        method: 'GET',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        type: JSON
      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    }

    this.get_invoice_details_by_user_id = function($http, $q, user_id) {
      var apiPath = UtilService.get_master_api() +
        '/get/invoice/details/by/user_id/' + user_id;

      var deferred = $q.defer();
      $http({
        method: 'GET',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        type: JSON
      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    }

    this.get_invoice_amount = function($http, $q, dataobj) {

      var apiPath = UtilService.get_master_api() +
        '/get/month_wise/invoice_amount';

      var deferred = $q.defer();
      $http({
        method: 'POST',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        data: dataobj,
        ContentType: 'application/json'

      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    }

    this.get_all_data = function($http, $q, dataobj) {

      var apiPath = UtilService.get_master_api() +
        '/get/all_month_details';

      var deferred = $q.defer();
      $http({
        method: 'POST',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        data: dataobj,
        ContentType: 'application/json'

      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    }

    this.insert_pos_details = function($http, $q, dataobj) {

      var apiPath = UtilService.get_master_api() +
        '/insert_pos_details';

      var deferred = $q.defer();
      $http({
        method: 'POST',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        data: dataobj,
        ContentType: 'application/json'

      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    }

    this.get_data_by_invoice_numb = function(dataobj) {
      var apiPath = UtilService.get_api() + '/login/get/invoice_details/by/invoice';
      var deferred = [];
      jQuery.ajax({
        method: 'POST',
        //  headers: {token: UtilService.get_from_localstorage('token')},
        url: apiPath,
        async: false,
        data: { invoice_number: dataobj },
        ContentType: 'application/json'
      }).done(function(data) {
        deferred = (data);
      }).error(function(data) {
        deferred = ("Data Error");
      })
      return deferred;
    }


    this.upload_invoice_data = function(dataobj) {

      var apiPath = UtilService.get_master_api() + '/invoice/csv/upload';
      var deferred = [];
      jQuery.ajax({
        method: 'POST',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        async: false,
        data: { all_data: dataobj },
        ContentType: 'application/json'
      }).done(function(data) {
        deferred = (data);
      }).error(function(data) {
        deferred = ("Data Error");
      })
      return deferred;
    }


  });

/**
 * Created by dev11 on 1/27/2016.
 */

'use strict';

angular.module('vstford')
  .service('PaymentService', function Menu(UtilService) {
    this.get_invoice_list = function($http, $q) {

      //            var apiPath = UtilService.get_master_api() +
      //                '/get/invoice/list';

      var apiPath = UtilService.get_api() +
        '/login/get/invoice/list';
      var token = UtilService.get_from_localstorage('token');

      var deferred = $q.defer();
      $http({
        method: 'POST',
        // headers: {token: UtilService.get_from_localstorage('token')},
        url: apiPath,
        type: JSON

      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    };

    this.get_invoice_list_by_retailer = function($http, $q, objdata) {
      var apiPath = UtilService.get_master_api() + '/get/invoice/list/by/retailer';
      var deferred = $q.defer();
      $http({
        method: 'POST',
        headers: { token: UtilService.get_from_localstorage('token') },
        url: apiPath,
        data: objdata,
        type: JSON
      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    };


    this.add_payment_upload = function($http, $q, dataobj) {

      //            var apiPath = UtilService.get_master_api() +
      //                '/get/invoice/list';

      //var apiPath = UtilService.get_api() +
      //  '/insert/retailer/payment';
      //var token = UtilService.get_from_localstorage('token');

      var apiPath = UtilService.get_api() + '/login/insert/retailer/payment';

      var deferred = $q.defer();
      $http({
        method: 'POST',
        // headers: {token: UtilService.get_from_localstorage('token')},
        url: apiPath,
        data: dataobj,
        ContentType: 'application/json'

      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    };

    this.get_details_by_invo_numb = function($http, $q, dataobject) {

      var apiPath = UtilService.get_api() + '/login/get/payment/details';

      var deferred = $q.defer();
      $http({
        method: 'POST',
        // headers: {token: UtilService.get_from_localstorage('token')},
        url: apiPath,
        data: dataobject,
        ContentType: 'application/json'

      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      });
      return deferred.promise;
    };

    this.get_company_name_list = function($http, $q, retailer_search) {
      var apiPath = UtilService.get_api() + '/login/get/company/name';

      var deferred = $q.defer();
      $http({
        method: 'POST',
        // headers: {token: UtilService.get_from_localstorage('token')},
        url: apiPath,
        data: { retailer_search: retailer_search },
        ContentType: 'application/json'

      }).success(function(data) {
        deferred.resolve(data);
      }).error(function(data) {
        deferred.reject("Data Error");
      })
      return deferred.promise;
    }

        this.get_retailer_si_by_typeid = function($http, $q, retailer_search) {
            var apiPath = UtilService.get_master_api() + '/retailer_si_by_typeid';

            var deferred = $q.defer();
            $http({
                method: 'POST',
                headers: {token: UtilService.get_from_localstorage('token')},
                url: apiPath,
                data: { "type_id": 30, "participant_code":retailer_search },
                ContentType: 'application/json'

            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data) {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }
             this.get_si_by_retailerid = function($http, $q, retailer_id) {
                 var apiPath = UtilService.get_master_api() + '/si_by_retailerid';

                 var deferred = $q.defer();
                 $http({
                     method: 'POST',
                     headers: {token: UtilService.get_from_localstorage('token')},
                     url: apiPath,
                     data: { "retailer_id": retailer_id },
                     ContentType: 'application/json'

                 }).success(function(data) {
                     deferred.resolve(data);
                 }).error(function(data) {
                     deferred.reject("Data Error");
                 })
                 return deferred.promise;
             }

        this.get_data_by_invoice_numb = function(dataobj) {
      var apiPath = UtilService.get_api() + '/login/get/invoice_details/by/invoice';
      var deferred = [];
      jQuery.ajax({
        method: 'POST',
        //  headers: {token: UtilService.get_from_localstorage('token')},
        url: apiPath,
        async: false,
        data: { invoice_number: dataobj },
        ContentType: 'application/json'
      }).done(function(data) {
        deferred = (data);
      }).error(function(data) {
        deferred = ("Data Error");
      })
      return deferred;
    }


    this.add_payment_upload_from_csv = function(dataobj) {
      var apiPath = UtilService.get_api() + '/login/insert/retailer/payment/from_csv';
      var deferred = [];
      jQuery.ajax({
        method: 'POST',
        //  headers: {token: UtilService.get_from_localstorage('token')},
        url: apiPath,
        async: false,
        data: dataobj,
        ContentType: 'application/json'
      }).done(function(data) {
        deferred = (data);

      }).error(function(data) {
        deferred = ("Data Error");


      })
      return deferred;
    };
    //            var apiPath= UtilService.get_api() +'/login/insert/retailer/payment/from_csv';
    //
    //            var deferred = $q.defer();
    //            $http({
    //                method: 'POST',
    //                // headers: {token: UtilService.get_from_localstorage('token')},
    //                url: apiPath,
    //                data: {full_csv_data:dataobj},
    //                ContentType: 'application/json'
    //
    //            }).success(function (data) {
    //                deferred.resolve(data);
    //            }).error(function (data)
    //            {
    //                deferred.reject("Data Error");
    //            })
    //            return deferred.promise;
    //        }



  });

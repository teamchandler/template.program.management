/**
 * Created by Hiru on 3/11/2015.
 */

'use strict';


angular.module('vstford')
    .service('ProfileService', function MasterService(UtilService) {

        this.profile_basic_update = function($http,$q,profile_basic){

            var apiPath =  UtilService.get_master_api() + '/user/basic/info/update' ;
            var deferred = $q.defer();
            $http({
                method: 'POST',
                headers: {token: UtilService.get_from_localstorage('token')},
                url: apiPath,
                data:profile_basic,
                ContentType: 'application/json'
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;

        }

        this.get_profile_info = function($http, $q, email_id ){
            var apiPath = UtilService.get_master_api() +
                '/userinfo/by/user/'+ email_id ;

            var deferred = $q.defer();
            $http({
                method: 'GET',
                headers: {token: UtilService.get_from_localstorage('token')},
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }

        this.update_profile_address = function($http, $q, dataobj){

            var apiPath =  UtilService.get_master_api() +
                '/user/basic/address/update' ;

            var deferred = $q.defer();
            $http({
                method: 'POST',
                headers: {token: UtilService.get_from_localstorage('token')},
                url: apiPath,
                data: dataobj,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }


    });


'use strict';

angular.module('vstford')
  .service('CryptoService', function CryptoService(UtilService) {

        this.encrypt = function (m){
//            console.log(CryptoJS.AES.encrypt(m, UtilService.get_encryption_phrase()).toString());
            return CryptoJS.AES.encrypt(m, UtilService.get_encryption_phrase()).toString();
        }
        this.decrypt = function (m){
            return CryptoJS.AES.decrypt(m, UtilService.get_encryption_phrase().toString(CryptoJS.enc.Utf8));
        }
  });

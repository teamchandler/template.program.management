/**
 * Created by Developer8 on 1/28/2016.
 */

'use strict';

angular.module('vstford')
  .controller('PaymentCtrl', function($scope, $state, $http, $q, $upload,
                                      NotificationService, UtilService, MasterService, $timeout, $rootScope, PaymentService) {

    $scope.invoice_details_pop_show = false;

    function init() {
      //get_company_name_list();
      //get_invoice_list();

    }

    $scope.invo_data = {};

    $scope.get_auto_complete_retailer_values = function() {
      var search_distributor = $scope.invo_data.retailer_name;
      if (search_distributor.length > 2) {
        get_company_name_list();
      }
    };

    $scope.check_invoices = function() {
      $scope.invoice_details_pop_show = true;
    }

    $scope.invoice_pop_up_close = function() {
      $scope.invoice_details_pop_show = false;
    }


    function get_company_name_list() {
      var retailer_search = "";
      if ($scope.invo_data.retailer_name) {
        var choose_retailer = $scope.invo_data.retailer_name;
        var ret_data = choose_retailer.split(" - ");
        if (ret_data.length > 1) {
          retailer_search = ret_data[0];
        } else {
          retailer_search = $scope.invo_data.retailer_name;
        }
      }

      UtilService.show_loader();
      PaymentService.get_company_name_list($http, $q, retailer_search)
        .then(function(data) {
            UtilService.hide_loader();
            if (data.length > 0) {
              $scope.retailer_list = data;
            } else {
              $scope.retailer_list = [];
            }
          },
          function() {
            UtilService.hide_loader();
            //Display an error message
            $scope.error = error;

          });

    };

    $scope.populate_invoice_number = function() {

      var obj_ret = {};

      if ($scope.invo_data.retailer_name) {
        var choose_retailer = $scope.invo_data.retailer_name;
        var ret_data = choose_retailer.split(" - ");
        if (ret_data.length > 1) {
          obj_ret = { "retailer_code": ret_data[0] }
        } else {
          obj_ret = { "retailer_code": $scope.invo_data.retailer_name }
        }
      }

      UtilService.show_loader();

      PaymentService.get_invoice_list_by_retailer($http, $q, obj_ret)
        .then(function(data) {
            UtilService.hide_loader();
            //$scope.invoice_list = data;
            if (data.length > 0) {
              $scope.invoice_list = data;
            } else {
              $scope.invoice_list = [];
            }
          },
          function() {
            UtilService.hide_loader();
            //Display an error message
            $scope.error = error;

          });

    }

    $scope.pop_inv_data = function(oi) {
      //console.log(oi);
      //$scope.invo_data = oi;
      var it = new Date(oi.invoice_date);
      $scope.invo_data.invoice_amount = oi.invoice_amount;
      var d = it.getDate();
      var m = it.getMonth() + 1;
      var y = it.getFullYear();
      //console.log(d, m, y);
      if (d < 9) {
        d = '0' + d;
      }
      if (m < 9) {
        m = '0' + m;
      }
      $scope.invo_data.invoice_date = d + '/' + m + '/' + y;
      $scope.invo_data.invoice_number =  oi.invoice_number;
      $scope.invoice_details_pop_show = false;
    }

    function get_invoice_list_by_retailer() {
      UtilService.show_loader();
      PaymentService.get_invoice_list_by_retailer($http, $q)
        .then(function(data) {
            UtilService.hide_loader();
            $scope.invoice_list = data;

            if (data.length > 0) {
              $scope.invoice_list = data;
            } else {
              $scope.invoice_list = [];
            }
          },
          function() {
            UtilService.hide_loader();
            //Display an error message
            $scope.error = error;

          });

    };

    $scope.get_autocomplete_values_for_invoice = function() {
      var search_distributor = $scope.invo_data.invoice_number;
      if (search_distributor.length > 2) {
        //get_invoice_list();
        $scope.populate_invoice_number();
      }
    };

    /**************Test Calender Panel*****************/
    $scope.today = function() {
      $scope.invo_data.payment_date = new Date();
    };
    $scope.today();

    $scope.clear = function() {
      $scope.invo_data.invoice_date = null;
    };

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
      return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
    };

    $scope.toggleMin = function() {
      $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open = function($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.opened = true;
    };

    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

    $scope.save_payment = function() {

      if ($scope.invo_data.payment_received == true) {
        $scope.invo_data.payment_received = "Y";
      } else {
        $scope.invo_data.payment_received = "N";
      }

      if ($scope.invo_data.retailer_name == "" || typeof $scope.invo_data.retailer_name === 'undefined') {
        alert("Please Enter Retailer Name");
        return false;
      } else if ($scope.invo_data.invoice_number == "" || typeof $scope.invo_data.invoice_number === 'undefined') {
        alert("Please Enter Invoice Number");
        return false;
      } else if ($scope.invo_data.amount_received == "" || typeof $scope.invo_data.amount_received === 'undefined') {
        alert("Please Enter Amount Recieved");
        return false;
      } else if ($scope.invo_data.payment_date == "" || typeof $scope.invo_data.payment_date === 'undefined') {
        alert("Please Enter Payment Date");
        return false;
      } else if ($scope.invo_data.payment_comment == "" || typeof $scope.invo_data.payment_comment === 'undefined') {
        alert("Please Enter Comment");
        return false;
      }

      Date.dateDiff = function(datepart, fromdate, todate) {
        datepart = datepart.toLowerCase();
        var diff = todate - fromdate;
        var divideBy = { w:604800000, d:86400000, h:3600000, n:60000, s:1000 };
        return Math.floor( diff/divideBy[datepart]);
      }
      //Set the two dates
      var idt=$scope.invo_data.invoice_date;
      var idt_data = idt.split("/");
      var inv_dt = new Date(2016, 0, 1);
      if(idt_data.length>0){
        var iy=parseInt(idt_data[2]);
        var im=parseInt(idt_data[1])-1;
        var id=parseInt(idt_data[0]);
        inv_dt = new Date(iy, im, id);
      }
      var pay_dt = new Date($scope.invo_data.payment_date);
      var diff=Date.dateDiff('d', inv_dt, pay_dt)

      alert(diff);

      if(diff>21){
        alert("Payment Date Should Not Be Greater Than 21 Days From The Date Of Invoice");
        return false;
      }

      $scope.invo_data.user_id = UtilService.get_from_localstorage('user_info').email_id;

      UtilService.show_loader();
      PaymentService.add_payment_upload($http, $q, $scope.invo_data)
        .then(function(data) {
            //  $scope.show_cond='s';
            UtilService.hide_loader();
            //$scope.issaveenable=true;
            alert("Payment Information Has Been Saved Successfully");
          },
          function() {
            UtilService.hide_loader();
            //Display an error message
            $scope.error = error;

          }
        );
    };

    $scope.clear_field = function() {
     // $scope.invo_data = {};
      $scope.invo_data.invoice_number = "";
      $scope.invo_data.invoice_date = "";
      $scope.invo_data.invoice_amount = "";
      $scope.invo_data.payment_received = false;
      $scope.invo_data.amount_received = "";
      $scope.invo_data.payment_comment = "";
      $scope.checking = false;

    };

    $scope.details = function() {

      var obj_inv = { invoice_number: $scope.invo_data.invoice_number };

      PaymentService.get_details_by_invo_numb($http, $q, obj_inv)
        .then(function(data) {
            UtilService.hide_loader();
            if (data.length > 0) {
              var it = new Date(data[0].invoice_date);
              $scope.invo_data.invoice_amount = data[0].invoice_amount;
              var d = it.getDate();
              var m = it.getMonth() + 1;
              var y = it.getFullYear();
              //console.log(d, m, y);
              if (d < 9) {
                d = '0' + d;
              }
              if (m < 9) {
                m = '0' + m;
              }
              $scope.invo_data.invoice_date = d + '/' + m + '/' + y;
            } else {
              $scope.invo_data.invoice_amount = "";
              $scope.invo_data.invoice_date = "";
            }
          },
          function() {
            UtilService.hide_loader();
            //Display an error message
            $scope.error = error;
            console.log(error);

          });

    };

    init();
  });

/**
 * Created by DRIPL_1 on 04/16/2015.
 */

'use strict';

angular.module('vstford')
    .controller('ClaimCtrl', function ($scope,$state,ClaimService,$http, $q, $upload,
                                       NotificationService, UtilService, MasterService,$timeout,$rootScope,PaymentService) {

        $scope.invo_data={};
        var dist = [];
        var distributor_data = [{
          name : "VST FORD",
          city : "Bangalore"
        }];

        $scope.distributor_list = distributor_data;

        $scope.get_auto_complete_retailer_values = function() {
            var search_distributor = $scope.invo_data.ret_obj;
            if (search_distributor.length > 2) {

                get_retailer_si_by_typeid();
            }
        };


        function get_retailer_si_by_typeid(){
            var retailer_search = "";
            if ($scope.invo_data.retailer_name) {
                var choose_retailer = $scope.invo_data.retailer_name;
                var ret_data = choose_retailer.split(" - ");
                if (ret_data.length > 1) {
                    retailer_search = ret_data[0];
                } else {
                    retailer_search = $scope.invo_data.retailer_name;
                }
            }

            UtilService.show_loader();
            PaymentService.get_retailer_si_by_typeid($http, $q, retailer_search)
                .then(function(data) {
                    UtilService.hide_loader();
                    if (data.length > 0) {
                        $scope.retailer_list = data[0];
                      console.log('retailer_list   '+ JSON.stringify($scope.retailer_list));
                    } else {
                        $scope.retailer_list = [];
                    }
                },
                function() {
                    UtilService.hide_loader();
                    //Display an error message
                    $scope.error = error;

                });

        };

     $scope.get_si_by_retailerid = function(){
          var retailer_id=  $scope.invo_data.ret_obj;
         var choose_retailer =  $scope.invo_data.ret_obj;
         var ret_data = choose_retailer.split(" - ");
         var retlr_id = ret_data[ret_data.length -1];
         $scope.retailer_name=  ret_data[ret_data.length -2];
         $scope.retailer_code=  ret_data[ret_data.length -3];
            UtilService.show_loader();
            PaymentService.get_si_by_retailerid($http, $q, retlr_id)
                .then(function(data) {
                    UtilService.hide_loader();
                    if (data.length > 0) {
                        $scope.si_list = data[0];
                        console.log('si_list   '+ JSON.stringify($scope.si_list));
                        console.log('invo_data.ret_obj   ' + JSON.stringify($scope.invo_data.ret_obj));
                    } else {
                        $scope.si_list = [];
                    }
                },
                function() {
                    UtilService.hide_loader();
                    //Display an error message
                    $scope.error = error;

                });

        };

        $scope.numeric_only = function (event) {
            // Allow only backspace ,delete and tab
            if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 190 || event.keyCode == 110 || (event.keyCode >= 96 && event.keyCode <= 105)) {

                // let it happen, don't do anything

            }
            else {
                // Ensure that it is a number and stop the keypress
                if (event.keyCode < 48 || event.keyCode > 57 ) {
                    event.preventDefault();

                }
            }
        };

        /**************Test Calender Panel*****************/
        $scope.today = function() {
            $scope.invo_data.invoice_date = new Date();
        };
        $scope.today();

        $scope.clear = function () {
            $scope.invo_data.invoice_date = null;
        };

        // Disable weekend selection
        $scope.disabled = function(date, mode) {
            return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
        };

        $scope.toggleMin = function() {
            $scope.minDate = $scope.minDate ? null : new Date();
        };
        $scope.toggleMin();

        $scope.open = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened = true;
        };

//        $scope.open = function() {
//
//            $timeout(function() {
//                $scope.opened = true;
//            });
//        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        $scope.initDate = new Date('2016-15-20');
        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];

        var get_upload_url = function(){

        }

        $scope.issaveenable=true;
        //  $scope.invo_data="";

//        $scope.$watch('invo_data.invoice_date', function(){
//
//            var start_date=UtilService.get_program_start_date();
//            var end_date= UtilService.get_program_end_date();
//            var inv_date=$scope.invo_data.invoice_date;
//
//            if(inv_date==start_date ||inv_date>start_date){
//
//                alert('ok');
//            }
//
//        });



        $scope.go_to_thanks = function() {
            $state.go("main.thank_you", {});
        }

        $scope.onFileSelect = function($files) {

            //$files: an array of files selected, each file has name, size, and type.
//            for (var i = 0; i < $files.length; i++) {

            var $file = $files[0];
//            console.log($file);
            UtilService.show_loader();
            $upload.upload({
                url: UtilService.get_master_api() +   "/aws/upload/file",
                headers: {token: UtilService.get_from_localstorage('token')},
                file: $file,
                progress: function (e) {
                }
            }).then(function (data, status, headers, config) {
                // alert(data.data) ;
                UtilService.hide_loader();
                $scope.issaveenable = false;
                console.log(data);
                // $scope.invo_data.supporting_doc="";
                $scope.invo_data.supporting_doc=data.data;
            });
//            MasterService.get_update_url($http, $q, $file.name)
//                .then(function (data) {
//                    console.log(data);
//                    MasterService.upload($http, $q, $file, $file.type, data)
//                        .then (function(response){
//                            console.log(response);
//                        },
//                        function () {
//                            //Display an error message
//                            $scope.error = error;
//
//                        });
//                },
//                function () {
//                    //Display an error message
//                    $scope.error = error;
//
//                });

//            }
        }
        function get_retailer_list(){
          $scope.distributor_list = distributor_data;
            //UtilService.show_loader();
            //ClaimService.get_retailer_list($http, $q)
            //    .then(function (data) {
            //        UtilService.hide_loader();
            //        $scope.distributor_list = data;
            //
            //        if (data[0].message != "no result found") {
            //            $scope.not_end_of_list = true;
            //        }
            //        else {
            //            $scope.s_data = data;
            //            $scope.end_of_list = false;
            //        }
            //    },
            //    function () {
            //        UtilService.hide_loader();
            //        //Display an error message
            //        $scope.error = error;
            //
            //    });

        }



        function get_invoice_list(){
            UtilService.show_loader();
            PaymentService.get_invoice_list($http, $q)
                .then(function (data) {
                    UtilService.hide_loader();
                    $scope.invoice_list = data;

                    if (data[0].message != "no result found") {
                        $scope.not_end_of_list = true;
                    }
                    else {
                        $scope.invoice_data = data;
                        $scope.end_of_list = false;
                    }
                },
                function () {
                    UtilService.hide_loader();
                    //Display an error message
                    $scope.error = error;

                });

        }



        $scope.get_autocomplete_values = function () {
            var search_distributor = $scope.invo_data.retailer;
            if (search_distributor.length > 2) {
                get_retailer_list();
            }
        }

        $scope.get_autocomplete_values_for_invoice = function () {
            var search_distributor = $scope.invo_data.invoice_number;
            if (search_distributor.length > 2) {
                get_invoice_list();
            }
        }



        function addDays(days,date) {
            var result = new Date(date);
            result.setDate(result.getDate() + days);
            return result;
        }



        $scope.save_invoice = function () {
            if ($scope.invo_data.retailer){
            var str = $scope.invo_data.retailer;
            var arr = str.split("-");
            var arr_data = arr[0];
            var arr_name = arr_data.trim();
        }
           // dist.push(arr_data);
            var distributor_data =_.where($scope.distributor_list,{name: arr_name});
            if(distributor_data.length == 0 ) {
               alert('Please select a Distributor from the list');
               return false;
            }
            else if (typeof $scope.invo_data === 'undefined' ){
                alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
                return false;
            }
            else if ($scope.invo_data.invoice_number == "" || typeof $scope.invo_data.invoice_number === 'undefined' ){
                alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
                return false;
            }
            else if ($scope.invo_data.invoice_amount == "" || typeof $scope.invo_data.invoice_amount === 'undefined' ){
                alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
                return false;
            }
            else if ($scope.invo_data.invoice_date == "" || typeof $scope.invo_data.invoice_date === 'undefined' ){
                alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
                return false;
            }
            else if ($scope.invo_data.retailer == "" || typeof $scope.invo_data.retailer === 'undefined'||$scope.invo_data.retailer == "Select Distributor" ){
                alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
                return false;
            }
            else if ( $scope.invo_data.supporting_doc == ""  || typeof $scope.invo_data.supporting_doc === 'undefined'){
                alert ("Please select the file");
                return false;
            }

            var inv_date=$scope.invo_data.invoice_date;

            console.log("Invoice Date");
            var added_date = addDays(20,inv_date);
            console.log(inv_date);
            console.log(added_date);

            var inv_date_date =parseInt(inv_date.getDate());
            var inv_date_month=parseInt(inv_date.getMonth())+1;
            var inv_date_year=parseInt(inv_date.getFullYear());

            var months = [];
            months[1] = "January";
            months[2] = "February";
            months[3] = "March";
            months[4] = "April";
            months[5] = "May";
            months[6] = "June";
            months[7] = "July";
            months[8] = "August";
            months[9] = "September";
            months[10] = "October";
            months[11] = "November";
            months[12] = "December";

            if ( (inv_date_month < 5) || (inv_date_month > 7) ) {
                alert("Invoice date should be between 1st May to 31st July, 2016");
                return false;
            }
            if ( inv_date_year !=2016 ) {
                alert("Invoice date should be between 1st May to 31st July 2016");
                return false;
            }

            if( ($scope.upload_mode=='onbehalf') && ($scope.user_type=='annectos admin'))
            {
                $scope.invo_data.user_id=JSON.parse($scope.invo_data.si).email_id;
                $scope.invo_data.company_name=JSON.parse($scope.invo_data.si).company_name;
                $scope.invo_data.region=JSON.parse($scope.invo_data.si).region;
                $scope.invo_data.retailer_code=$scope.retailer_code;
                $scope.invo_data.retailer_name=$scope.retailer_name;
                $scope.invo_data.uploaded_month = months[inv_date_month];
                $scope.invo_data.upload_from = 'w';

            }
            else{
            $scope.invo_data.user_id=UtilService.get_from_localstorage('user_info').email_id;
            $scope.invo_data.company_name=UtilService.get_from_localstorage('user_info').company_name;
            $scope.invo_data.region=UtilService.get_from_localstorage('user_info').schneider_region;
            $scope.invo_data.retailer_code=UtilService.get_from_localstorage('user_info').retailer_code;
            $scope.invo_data.retailer_name=UtilService.get_from_localstorage('user_info').retailer_name;
            $scope.invo_data.uploaded_month = months[inv_date_month];
            $scope.invo_data.upload_from = 'w';
            }
            // Newly Added
            $scope.invo_data.credit_period = added_date;
            console.log('invo_data   ' + JSON.stringify($scope.invo_data));
            UtilService.show_loader();
            ClaimService.save_invoice($http, $q, $scope.invo_data)
                .then(function (data) {
                    //  $scope.show_cond='s';
                    UtilService.hide_loader();
                    $scope.issaveenable=true;
                    alert(data);
                },
                function() {
                    UtilService.hide_loader();
                    //Display an error message
                    $scope.error = error;

                });
        }


        $scope.clear_field = function () {
           // $scope.invo_data="";
           // init();

            $scope.invo_data={};

        }

        $scope.show_sku_list = function () {
            $scope.list_pop_show=true;

           // $scope.showModal = !$scope.showModal;

        }

        $scope.pop_up_close = function () {
            $scope.list_pop_show=false;
        }


        function search_sku_list() {
//            $scope.list_pop_show=false;


            var srch_sku="";
            if ( $scope.sku == ""  || typeof $scope.sku === 'undefined'){
                srch_sku='undefined';
            }else{
                srch_sku= $scope.sku;
            }

            ClaimService.search_sku_list($http, $q,srch_sku )
                .then(function (data) {

                    $scope.sku_list = data;

                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });


        }



        var init = function(){
          //  UtilService.change_header($state.current.data.header_label);
           // UtilService.select_menu($state.current.name);
            $rootScope.title = $state.current.title;
            //get_retailer_list();
           // search_sku_list();
           // get_invoice_list();
            $scope.list_pop_show = false;

            $scope.upload_mode= UtilService.get_upload_mode();
            $scope.user_type= UtilService.get_user_type();
            $scope.adminLogin = false;
            if( ($scope.upload_mode=='onbehalf') && ($scope.user_type=='annectos admin'))
            {
                $scope.adminLogin = true;
            }
        }

        init();
    });







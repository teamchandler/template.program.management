'use strict';

angular.module('vstford')
  .controller('LoginLoginCtrl', function (  $scope, $http,$q, $state,
                                            UtilService,LoginService,$rootScope
                                            ) {


        $scope.terms = {};

        $scope.login = function(){

            if ($scope.user == "" || typeof $scope.user === 'undefined' ){
                alert ("Please enter User Id & Password");
                return false;
            }
            else if ($scope.user.email_id == "" || typeof $scope.user.email_id === 'undefined' ){
                alert ("Please enter User Id");
                return false;
            }
            else if ($scope.user.password == "" || typeof $scope.user.password === 'undefined'){
                alert ("Please enter Password");
                return false;
            }
            $scope.user.company = UtilService.get_company();
            UtilService.show_loader();
            LoginService.user_login($http, $q, $scope.user)
                .then
            (
                function (data) {
                    UtilService.hide_loader();
//                    console.log(data);

                    if (data[0][0].msg ==='login successful') {

                        UtilService.set_to_localstorage("user_info", data[0][0]);
                        UtilService.set_to_localstorage("token", data[0][0].token);

                        var u = UtilService.get_from_localstorage("user_info");


                        if(u!=null) {
                            for (var i = 0; i < annectos_program_admin.length; i++) {
                                if (u.email_id != null && u.email_id != "") {
                                    if (u.email_id.toLowerCase() == annectos_program_admin[i]) {
                                        //  $scope.type_of_user = "annectos admin";
                                        user_type = "annectos admin";
                                        $rootScope.$broadcast('admin_login');

                                    }
                                }

                            }

                            for (var i = 0; i < schneider_program_admin.length; i++) {
                                if (u.email_id != null && u.email_id != "") {
                                    if (u.email_id.toLowerCase() == schneider_program_admin[i]) {
                                        // $scope.type_of_user = "schneider admin";
                                        user_type = "schneider admin";
                                        $rootScope.$broadcast('admin_login');
                                        break;
                                    }
                                }
                            }

                            for (var i = 0; i < rsm_admin.length; i++) {
                                if (u.email_id != null && u.email_id != "") {
                                    if (u.email_id.toLowerCase() == rsm_admin[i]) {
                                        // $scope.type_of_user = "schneider admin";
                                        user_type = "rsm admin";
                                        $rootScope.$broadcast('rsm_login');
                                        break;
                                    }
                                }
                            }

                            if(user_type=="schneider user") {
                              if(parseInt(u.login_user_type)==10){
                                $rootScope.$broadcast('company_login');
                              }
                              else if(parseInt(u.login_user_type)==20){
                                $rootScope.$broadcast('distributor_login');
                              }
                              else{
                                $rootScope.$broadcast('user_login');
                              }
                            }

                        }

                    }
                    else{
                        UtilService.hide_loader();
                        alert("Sorry - your credentials don't match.")
                        UtilService.set_to_localstorage("user_info", "");

                    }

                },

                function () {
                    //Display an error message
                    UtilService.hide_loader();
                    $scope.error = error;
                }
            );

        }


        $scope.$on('admin_login', function () {
           $state.go("main.prog_admin");
        });


        $scope.$on('company_login', function () {
          $state.go("main.company");
        });

        $scope.$on('distributor_login', function () {
          $state.go("main.distributor");
        });

        // For new user type

        $scope.$on('rsm_login', function () {
            $state.go("main.prog_admin");
        });


        $scope.$on('user_login', function () {
            $state.go("main.home");
        });


        $scope.terms_condition_updt = function(){
            var user_data = UtilService.get_from_localstorage("user_info");
            $scope.terms.email_id= user_data.email_id;
            $scope.terms.terms_status=1;
            $scope.terms.terms_date=new Date();
            LoginService.user_terms_condition_update($http, $q, $scope.terms)
                .then
            (
                function (data) {
//                    console.log(data);
                    if(data=="T&C Updated"){
                        $state.go("main.profile");
                    }


                },

                function () {
                    //Display an error message
                    $scope.error = error;
                }
            );

        }

        init();
        function init() {
            $rootScope.title = $state.current.title;
            $scope.poptrue=false;
            UtilService.set_to_localstorage("user_info", "");
            UtilService.set_to_localstorage("token", "");
            user_type = "schneider user";
//            UtilService.change_header($state.current.data.header_label);
//            UtilService.select_menu($state.current.name);
        };


  });

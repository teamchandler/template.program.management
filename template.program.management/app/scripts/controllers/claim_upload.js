/**
 * Created by dev11 on 2/8/2016.
 */


'use strict';

angular.module('vstford')
    .controller('ClaimUploadCtrl',function ($scope,$http,$q,UtilService,ClaimService,$window) {

        $scope.payment_arry=[];

        $scope.showContent = function($fileContent) {

            $scope.content = $fileContent;


            var full_csv_data = [];
            var str = $scope.content;
            var column_data = [];
            var myarray = str.split(/\n/g);
            myarray.splice((myarray.length - 1), 1);

            function putting_data_in_grid() {
                UtilService.show_loader();

                for (var i = 0; i < myarray.length; i++) {

                    var csv_obj = {};
                    var string = myarray[i];
                    column_data = string.split(';');
                    if (i != 0) {

                        csv_obj = {user_id:column_data[0],company_name: column_data[1], invoice_number: column_data[2],invoice_date: column_data[3], retailer: column_data[4], invoice_amount: column_data[5]};
                        $scope.payment_arry.push(csv_obj);
                    }


                }
                // Checking if the invoice exist in DB or Not

                for (var j = 0; j < $scope.payment_arry.length; j++) {
                    var data = $scope.payment_arry[j].invoice_number;
                    var data2 = ClaimService.get_data_by_invoice_numb(data);
                    if (data2.err) {
                        UtilService.hide_loader();
                        console.log(data2.err);
                        alert("Data Error");
                    }
                    else {
                        if (data2.length > 0) {
                            $scope.payment_arry[j].exist_in_db = "Y";
                        }
                        else {
                            $scope.payment_arry[j].exist_in_db = "N";
                        }
                        UtilService.hide_loader();
                        console.log(data2);
                    }
                }
                UtilService.hide_loader();
            }

            if($scope.payment_arry.length>0){

                $scope.payment_arry=[];
                putting_data_in_grid();

            }
            else
            {
                putting_data_in_grid();
            }

        }


        function addDays(days,date) {
            var result = new Date(date);
            result.setDate(result.getDate() + days);
            return result;
        }


        $scope.process_all_data = function () {

            for (var i = 0; i < $scope.payment_arry.length; i++) {
                var added_date = addDays(20,$scope.payment_arry[i].invoice_date);
                $scope.payment_arry[i].credit_period = added_date;
                var data = $scope.payment_arry[i];
                var data1 = ClaimService.upload_invoice_data(data);
                if (data1.err) {
                    UtilService.hide_loader();
                    alert("Data Error");
                }
                else {


                }
            }

            alert("All data has been inserted.");
        }

        var init = function(){

        }

        init();

    });



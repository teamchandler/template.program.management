/**
 * Created by DRIPL_1 on 04/23/2015.
 */


'use strict';

angular.module('vstford')
    .controller('resetpasswordCtrl', function ($scope,$http,$q,UtilService,LoginService,$state,localStorageService,$stateParams) {

        //Change previous password and set new password


        $scope.resetpwd = {};
        $scope.resetpwd.email_id = $stateParams.id;
        $scope.resetpwd.guid = $stateParams.cid;


        $scope.changePassword = function () {

            if ($scope.registration.password !=  $scope.registration.confrmpwd )
            {
                alert('Password and Confirm Password needs to be same');
                return false;
            }


            if ($scope.registration.password == $scope.registration.confrmpwd) {
                $scope.registration.email_id = $scope.resetpwd.email_id;
                $scope.isViewLoading = true;
                var returnval = '';
                //returnval = registrationService.UserRegistration($http, $q, $scope.registration);
                $scope.registration.company = UtilService.get_company();
                UtilService.show_loader();
                LoginService.SaveNewPassword($http, $q, $scope.registration).then(function (data) {
                        UtilService.hide_loader();
                        localStorageService.set('message', JSON.parse(data.toString()));
                        $scope.isViewLoading = false;
                       // $location.path("/message");
                        alert("Your Password has been changed");
                        $state.go('login.login');
                    },
                    function () {
                        //Display an error message
                        UtilService.hide_loader();
                        $scope.error = error;
                    });
            }
            else {

                localStorageService.set('message', 'Both password should be same. ');
              //  $location.path("/message");
            }

        };
        // alert($scope.registration_active);
        LoginService.resetPwdCheck($http, $q, $scope.resetpwd).then(function (data) {
                //Update UI using data or use the data to call another service
                //alert("Your account successfully activated. Please login now");
                //alert("Your account successfully activated. Please login now");
                //$location.path("/login");
                if(data.toString()=="You Can Not Reset Your Password With This Link"){
                    // $location.path("/message");
                    $state.go('login.login');
                }
            },
            function(){
                //Display an error message
                $scope.error= error;
            });

    });



'use strict';

angular.module('vstford')
  .controller('RedemptiondetailCtrl', function ($scope,$http,$q,UtilService,MasterService,$state,$stateParams,$rootScope) {

        $scope.pop_up_close=function(){

            $state.go("main.redemption");
        }
        var order_id =$stateParams.order_id;
        var status=$stateParams.status;
        $scope.status=status;
        $scope.order_date=$stateParams.order_date;
        $scope.points=$stateParams.points;
        init();

        function init() {
            $rootScope.title = $state.current.title;
            get_redemptionmore_details();
            get_order_information();
        }

        //Get Redemption More Detail
        function get_redemptionmore_details() {
            UtilService.show_loader();
            MasterService.get_redemptionmore_details($http, $q,order_id).then(function (data) {
                    UtilService.hide_loader();
                    $scope.redemptionmore_details = data[0][0];
                    $scope.shipping_adress= $scope.redemptionmore_details.user_id +"/br"+  $scope.redemptionmore_details.state +" "+ $scope.redemptionmore_details.pincode;



                },
                function () {
                    //Display an error message
                    UtilService.hide_loader();
                    $scope.error = error;
                    $scope.loading = false;
                });


        }

        //Get Order Information Grid
        function get_order_information() {
            UtilService.show_loader();
            MasterService.get_order_information($http, $q,order_id).then(function (data) {
                    // $scope.order_information_details =  $scope.order_information;
                    UtilService.hide_loader();
                    $scope.order_information= JSON.parse(data[0][0].cart_data);
                    // $scope.shipping_adress= $scope.redemptionmore_details.user_id +"/br"+  $scope.redemptionmore_details.state +" "+ $scope.redemptionmore_details.pincode;



                },
                function () {
                    //Display an error message
                    UtilService.hide_loader();
                    $scope.error = error;
                    $scope.loading = false;
                });


        }

    });

'use strict';

angular.module('vstford')
  .controller('changepassCtrl', function ($scope,$http,$q,UtilService,LoginService,$state) {

        //Change previous password and set new password
        $scope.change_password = function(){
            var credential={};
            credential.company = UtilService.get_company();
            credential.email_id = $scope.email_id;
            credential.old_password = $scope.old_password;
            credential.new_password = $scope.new_password;
            UtilService.show_loader();
            LoginService.change_password($http, $q, credential)
                .then
            (
                function (data) {
                    UtilService.hide_loader();
                    if (data[0][0].msg ==='password updated'){
                        alert("You have successfully changed your password. Please login with new credentials now.");
                        UtilService.set_to_localstorage("user_info", "");
                        $state.go("login.login");
                    }
                    else{

                        UtilService.set_to_localstorage("user_info", "");
                        alert(data[0][0].msg);
                    }
                },

                function () {
                    UtilService.hide_loader();
                    //Display an error message
                    $scope.error = error;
                }
            );

        }

        init();

        function init()
        {
            $scope.username = "";
            $scope.old_password = "";
            $scope.new_password = "";
            var user_data = UtilService.get_from_localstorage("user_info");
            $scope.email_id= user_data.email_id;


        };
  });

/**
 * Created by Developer9 on 3/16/2015.
 */
'use strict';

angular.module('vstford')
    .controller('LoaderCtrl', function ($scope,$state,
                                        UtilService,  NotificationService) {

        function init(){

            $scope.loading=false;
        }

        $scope.$on('show loader', function() {
            $scope.loading = NotificationService.loader_condition;
        });

        $scope.$on('hide loader', function() {
            $scope.loading = NotificationService.loader_condition;
        });


    });

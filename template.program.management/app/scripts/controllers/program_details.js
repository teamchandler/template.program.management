/**
 * Created by Hassan on 04/19/2016.
 */

'use strict'

angular.module('vstford')
  .controller('programdetailCtrl', function($scope, $state, $stateParams, UtilService,
                                            $http, $q, ClaimService, $location, $rootScope) {

    var user_id = $stateParams.user_id;
    var claim_id = $stateParams.claim_id;
    var login_user_type = UtilService.get_user_type();
    var inv_url = $location.absUrl();
    $scope.master_data = true;

    $scope.numeric_only = function(event) {
      // Allow only backspace and delete
      if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 190 || event.keyCode == 9 || (event.keyCode >= 96 && event.keyCode <= 105)) {
        // let it happen, don't do anything
      } else {
        // Ensure that it is a number and stop the keypress
        if ((event.keyCode < 48 || event.keyCode > 57)) {
          event.preventDefault();

        }
      }
    };

    $scope.invoice_details_pop_show_1 = false;

    /*Start*************Test Calender Panel*****************/
    $scope.today = function() {
      $scope.invoice_date = new Date();
    };
    $scope.today();

    $scope.clear = function() {
      $scope.invoice_date = null;
    };

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
      return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
    };

    $scope.toggleMin = function() {
      $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open = function($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.opened = true;
    };

    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

    /*End*************Test Calender Panel*****************/


    $scope.user_type = login_user_type;

    init();

    function init() {
      console.log($state.current);
      console.log($stateParams.claim_id);
      $scope.invamntdisabled = false;
      $scope.invamntexclvatdisabled = false;
      $rootScope.title = $state.current.title;
      get_retailer_list();
      invoice_details_by_number(claim_id);
      get_invoice_details_by_user_id(user_id);
      //get_sku_list();
      check_user_type();
    }

    function get_retailer_list() {
      UtilService.show_loader();
      ClaimService.get_retailer_list($http, $q)
        .then(function(data) {
            UtilService.hide_loader();
            $scope.distributor_list = data;
            if (data[0].message != "no result found") {
              $scope.not_end_of_list = true;
            } else {
              $scope.s_data = data;
              $scope.end_of_list = false;
            }
          },
          function() {
            //Display an error message
            UtilService.hide_loader();
            $scope.error = error;

          });

    }

    var prev_inv_date = '';

    function invoice_details_by_number(claim_id) {
      UtilService.show_loader();
      ClaimService.invoice_details_by_number($http, $q, claim_id)
        .then(function(data) {
            UtilService.hide_loader();
            $scope.inv_detail = data[0];

            var inv_dt = new Date(data[0].invoice_date);

            var dt = inv_dt.getDate() * 1;
            if (dt < 10) {
              dt = "0" + dt;
            }
            var mon = (inv_dt.getMonth() * 1 + 1);
            if (mon < 10) {
              mon = "0" + mon;
            }
            var invoice_date = dt + "/" + mon + "/" + inv_dt.getFullYear();
            prev_inv_date = invoice_date;
            //                    alert(invoice_date);
            $scope.invoice_date = invoice_date;

            $scope.sku_details = data[0].claim_details;
            sku_array = $scope.sku_details;
            $scope.comment_list = data[0].approval_comments;
            $scope.amount_list = data[0].amount_track;

            $scope.inv_date_list = data[0].inv_date_track;
            $scope.status = data[0].status;

            if (data[0].status == "Verified & Approved") {
              $scope.isenable = true;
            } else if (data[0].status == "Rejected") {
              $scope.isenable = true;
            } else {
              $scope.isenable = false;
            }

            if (login_user_type == "schneider admin") {
              if (data[0].status == "VST Verification Required") {
                $scope.isenable = false;
              } else {
                $scope.isenable = true;
              }
            } else if (login_user_type == "rsm admin") {

              $scope.isenable = true;

            }

          },
          function() {
            UtilService.hide_loader();
            //Display an error message
            $scope.error = error;
          }
        );
    }

    function get_invoice_details_by_user_id(user) {

      ClaimService.get_invoice_details_by_user_id($http, $q, user)
        .then(function(data) {
            $scope.inv_details_user_id = data;
          },
          function() {
            //Display an error message
            $scope.error = error;

          });

    }

    $scope.get_autocomplete_values = function() {
      var search_sku = $scope.claim_details.sku;
      if (search_sku.length > 2) {
        get_sku_list_by_search_key();
      }
    }

    function get_sku_list_by_search_key() {
      var search_sku = $scope.claim_details.sku;
      ClaimService.get_sku_list_by_search_key($http, $q, search_sku)
        .then(function(data) {
            $scope.part_code_list = data;
          },
          function() {
            //Display an error message
            $scope.error = error;
          });
    }

    $scope.get_sel_sku_data = function() {

      var sku = $scope.claim_details.sku;
      if (sku != "") {
        ClaimService.search_by_part_code($http, $q, sku)
          .then(function(data) {
              console.log(data);
              if (data.length > 0) {
                $scope.claim_details = data[0];
                $scope.master_data = true;
              } else {
                $scope.claim_details = {};
                $scope.master_data = false;
              }
            },
            function() {
              //Display an error message
              $scope.error = error;
            });
      }

    }

    var sku_array = [];

    $scope.calc_verified_amount = function() {

      var unit_price = 0;
      var qty = 0;
      var disc = 0;
      var amt = 0;
      var verified_amount = 0;
      var discounted_amount = 0;
      var calc_amount = 0;

      if ($scope.claim_details.unit_price) {
        if ($scope.claim_details.unit_price != "" && typeof $scope.claim_details.unit_price !== 'undefined' && $scope.claim_details.unit_price != null) {
          unit_price = $scope.claim_details.unit_price * 1;
        }
      }

      if ($scope.claim_details.quantity) {
        if ($scope.claim_details.quantity != "" && typeof $scope.claim_details.quantity !== 'undefined' && $scope.claim_details.quantity != null) {
          qty = $scope.claim_details.quantity * 1;
        }
      }

      if ($scope.claim_details.discount) {
        if ($scope.claim_details.discount != "" && typeof $scope.claim_details.discount !== 'undefined' && $scope.claim_details.discount != null) {
          disc = $scope.claim_details.discount * 1;
        }
      }

      if (unit_price != "" && typeof unit_price !== 'undefined' && qty != "" && typeof qty !== 'undefined') {
        if ($scope.claim_details.discount && $scope.claim_details.discount != "" && typeof $scope.claim_details.discount !== 'undefined') {
          disc = $scope.claim_details.discount * 1;
          verified_amount = parseInt(qty) * unit_price;
          discounted_amount = ((verified_amount * disc) / 100);
          calc_amount = (verified_amount - discounted_amount);
        } else {
          calc_amount = parseInt(qty) * unit_price;
        }
        $scope.claim_details.amount = calc_amount.toFixed(2);
      }

    }

    //Add SKU In The Claim Details
    $scope.add_sku = function() {

      // get_all_sku_details();
      if (!$scope.claim_details) {
        alert("Please Enter Part Code Details!");
        return false;
      } else if ($scope.claim_details.sku == "" || typeof $scope.claim_details.sku === 'undefined') {
        alert("Please Enter Part Code!");
        return false;
      } else if ($scope.claim_details.quantity == "" || parseInt($scope.claim_details.quantity) == 0 || typeof $scope.claim_details.quantity === 'undefined') {
        alert("Please Enter Quantity");
        return false;
      } else if ($scope.claim_details.unit_price == "" || typeof $scope.claim_details.unit_price === 'undefined') {
        alert("Please Enter Unit Price");
        return false;
      }


      var sku = $scope.claim_details.sku.toUpperCase();
      var description = $scope.claim_details.description;
      var model = $scope.claim_details.model;

      var is_free = 0;

      if ($scope.claim_details.free) {
        is_free = 1;
      } else {
        is_free = 0;
      }
      var found = false;
      angular.forEach(sku_array, function(item) {
        if (sku != "PART CODE NEEDED") {
          if (item.sku == sku) {
            alert('Already Exists This Part Code');
            found = true;
          }
        }
      });

      if (found == false) {

        ClaimService.search_by_part_code($http, $q, sku)
          .then(function(data) {
              //$rootScope.$broadcast(data);
              var sku_status = "verified";
              if (data.length == 0) {
                sku_status = "not-verified";
              }
              var disc = 0;
              if ($scope.claim_details.discount != "" && $scope.claim_details.discount != null && typeof $scope.claim_details.discount !== 'undefined') {
                disc = $scope.claim_details.discount;
              }
              var Val = true;
              var Val1 = true;

              if ($scope.quantity > 10) {
                Val = confirm("You have entered more than 10 quantity , are you sure want to add this much part code ?");
              }

              if (!Val) {
                return false;
              }

              Val1 = confirm("You are adding " + sku + " .are you sure you want to add this part code?");

              if (!Val1) {
                return false;
              }

              $scope.sku_status_class = sku_status;
              var sku_details = {
                sku: sku,
                status: sku_status,
                description: description,
                quantity: $scope.claim_details.quantity,
                unit_price: $scope.claim_details.unit_price,
                amount: $scope.claim_details.amount,
                discount: disc,
                is_free: is_free,
                model: model
              };
              sku_array.push(sku_details);
              $scope.sku_details = sku_array;
              $scope.claim_details = {};
              var total_amount = 0;
              var total_qty = 0;
              var x;
              for (x = 0; x < sku_array.length; x++) {
                if (sku_array[x].status == 'verified') {
                  total_amount = total_amount + parseFloat(sku_array[x].amount);
                  total_qty = total_qty + parseInt(sku_array[x].quantity);
                }
              }

              $scope.all_total_amount = total_amount;
              $scope.all_total_quantity = total_qty;
              $scope.inv_detail.invoice_amount_excl_vat = total_amount;

            },
            function() {
              //Display an error message
              $scope.error = error;

            });
      }

    }

    //Delete SKU
    $scope.delete_by_sku = function(osku) {

      var sku_length = sku_array.length;
      for (var i = 0; i < sku_length; i++) {
        if (sku_array[i].sku == osku.sku) {
          if ($scope.inv_detail.verified_amount != 0 || $scope.inv_detail.verified_amount != "" || typeof $scope.inv_detail.verified_amount !== 'undefined') {
            $scope.inv_detail.verified_amount = parseFloat($scope.inv_detail.verified_amount - osku.amount);
          }
          $scope.all_total_quantity = parseFloat($scope.all_total_quantity - osku.quantity);
          $scope.all_total_amount = parseFloat($scope.all_total_amount - osku.amount);
          $scope.inv_detail.invoice_amount_excl_vat = parseFloat($scope.all_total_amount - osku.amount);
          sku_array.splice(i, 1);
          break;
        }
      }

      $scope.sku_details = sku_array;

    }

    //Getting Class Based On SKU Status
    $scope.part_verify_status = function(sku_staus) {

      var part_class = "";
      if (sku_staus == "verified") {
        part_class = "part_no_verify_status";
      } else {
        part_class = "part_no_not_verify_status";
      }

      return part_class;

    }

    //Approve Or Reject Part Code
    $scope.approve_reject_by_sku = function(osku) {

      var sku_status = osku.status;
      var new_status = "";

      if (osku.status == "not-verified") {
        new_status = "verified";
      } else {
        new_status = "not-verified";
      }

      var upd_sku = {
        sku: osku.sku,
        status: new_status,
        description: osku.description,
        quantity: osku.quantity,
        unit_price: osku.unit_price,
        amount: osku.amount,
        discount: osku.discount,
        is_free: osku.is_free,
        model: osku.model
      };

      var sku_length = sku_array.length;
      for (var i = 0; i < sku_length; i++) {
        if (sku_array[i].sku == osku.sku) {
          sku_array[i] = upd_sku;
          break;
        }
      }

      $scope.sku_details = sku_array;

      if (sku_status == "not-verified") {

        var override_sku = {
          claim_id: claim_id,
          user_id: UtilService.get_from_localstorage('user_info').email_id,
          sku: osku.sku
        };

        ClaimService.add_override_sku($http, $q, override_sku)
          .then(function(data) {
              alert("Approved Part CODE.");
            },
            function() {
              //Display an error message
              $scope.error = error;
            }
          );
      }

    }

    $scope.insert_claim_details = function() {

      if (sku_array.length == 0) {
        alert('Add Part Code first');
        return false;
      }

      var status = "Verified & Approved";


      if (login_user_type != "schneider admin") {

        angular.forEach(sku_array, function(item) {
          if (item.status == "not-verified") {
            status = "Blaupunkt Verification Required";
          }
        });
      }

      var retVal1 = true;

      retVal1 = confirm("You Are About To Approve The Invoice,Are You Sure You Want To Continue?");

      if (!retVal1) {
        return false;
      }

      var retVal2 = true;

      // retVal2 = confirm("you will not be able to modify the record now. are you sure you want to continue?");

      retVal2 = confirm("The Verified Amount Is  " + $scope.inv_detail.invoice_amount_excl_vat * 1 + ", The Invoice Amount Is  " + $scope.inv_detail.invoice_amount * 1 + "  Sure Want To Approve?");

      if (!retVal2) {
        return false;
      }

      appr_rej_send(status);

    }

    function appr_rej_send(status) {

      var date = new Date();
      var inv_date_track = [];
      inv_date_track = $scope.inv_date_list;

      var inv_date = new Date($scope.invoice_date);
      var invoice_date = 'Invalid Date';

      if (prev_inv_date != $scope.invoice_date) {
        if (inv_date != 'Invalid Date') {
          invoice_date = inv_date;
          var inv_date_date = parseInt(inv_date.getDate());
          var inv_date_month = parseInt(inv_date.getMonth()) + 1;
          var inv_date_year = parseInt(inv_date.getFullYear());

          if (inv_date_month > 7 || inv_date_month < 5) {
            alert("Invoice Date Should Be In Between '01-05-2016' And '31-07-2016'");
            return false;
          } else if (inv_date_year != 2016) {
            alert("Invoice Date Should Be In Between '01-05-2016' And '31-07-2016'");
            return false;
          } else if (inv_date_date > 32 || inv_date_date < 1) {
            alert("Invoice Date Should Be In Between '01-05-2016' And '31-07-2016'");
            return false;
          }



          var inv_date_tracking = {
            "user_id": UtilService.get_from_localstorage('user_info').email_id,
            "inv_date": $scope.invoice_date,
            "track_date": date
          };
          inv_date_track.push(inv_date_tracking);
        }

      }

      if ($scope.approval_comments == "" || typeof $scope.approval_comments === 'undefined') {
        alert("Please Enter Comment");
        return false;
      }

      var inv_amount = $scope.inv_detail.invoice_amount * 1;
      var inv_amount_exl_vat = $scope.inv_detail.invoice_amount_excl_vat * 1
      var amount_track = [];
      amount_track = $scope.amount_list;


      var approval_comments = [];
      approval_comments = $scope.comment_list;
      var prev_inv_amount = 0;
      angular.forEach(amount_track, function(item) {
        prev_inv_amount = item.invoice_amount * 1;
      });
      var retVal = true;

      if (inv_amount != prev_inv_amount) {
        retVal = confirm("Do You Want To Change The Invoice Amount?");
      }

      if (retVal) {

        var user_comment_type = "Schneider User";
        if (login_user_type == "schneider admin") {
          user_comment_type = "Schneider";
        } else if (login_user_type == "annectos admin") {
          user_comment_type = "Program Center";
        }

        var approve_comment = {
          "comments": $scope.approval_comments,
          "user_type": user_comment_type,
          "user_id": UtilService.get_from_localstorage('user_info').email_id,
          "comment_date": new Date()
        };

        approval_comments.push(approve_comment);

        var amount_tracking = {
          "user_id": UtilService.get_from_localstorage('user_info').email_id,
          "track_date": date,
          "invoice_amount": inv_amount,
          "invoice_amount_excl_vat": inv_amount_exl_vat
        };
        amount_track.push(amount_tracking);


        if (status == "Rejected" || status == "VST Verification Required") {
          if (sku_array.length == 0) {
            sku_array = [];
          }
        }

        var modified_by = UtilService.get_from_localstorage('user_info').email_id;
        var modified_date = new Date();

        ClaimService.update_claim_details($http, $q, claim_id, inv_amount, sku_array, modified_by, modified_date, status, approval_comments, amount_track, inv_date_track, invoice_date, inv_amount_exl_vat)
          .then(function(data) {
              if (login_user_type == "schneider admin") {
                trigger_email($scope.approval_comments, status, modified_by, $scope.inv_detail.invoice_number, inv_url);
              } else if (status == "Verified & Approved") {
                alert('Thanks!The Specific Invoice is Updated.This invoice (or part of the invoice)is now approved.');
              } else if (status == "Rejected") {
                alert("Thanks!The Specific Invoice is Updated.This invoice (or part of the invoice) is now rejected.");
              } else {
                alert("System can't approve this invoice because of missing information.This will be forwarded for Schneider Approval.");
              }
              $scope.isenable = true;
              $scope.approval_comments = "";
            },
            function() {
              //Display an error message
              $scope.error = error;

            });
      }

    }

    function trigger_email(approval_comments, status, modified_by, invoice_number, inv_url) {

      var to_email = UtilService.get_to_email();

      var data = {
        "template": {
          "name": "Schneider_Activity.html",
          "contact": [{
            "head": { "emailid": to_email, "subject": "Schneider Activity" },
            "body": {
              "comment": approval_comments,
              "status": status,
              "email_id": modified_by,
              "invoice_no": invoice_number,
              "inv_url": inv_url
            }
          }]

        }
      };

      ClaimService.trigger_email_srvc($http, $q, data);

    }

    $scope.send_to_schneider = function() {

      var status = "VST Verification Required";

      angular.forEach(sku_array, function(item) {
        if (item.status == "not-verified") {
          status = "VST Verification Required";
        }
      });
      appr_rej_send(status);
    }

    $scope.back_to_program = function(status) {
      var status = $scope.status;
      $state.go("main.prog_admin_status", { 'status': status });
    }

    $scope.reject_claim_details = function() {

      var status = "Rejected";
      appr_rej_send(status);
    }

    $scope.user_wise_invoice_details = function() {
      $scope.invoice_details_pop_show_1 = true;
    }

    $scope.invoice_pop_up_close_1 = function() {
      $scope.invoice_details_pop_show_1 = false;
    }


    function check_user_type() {
      if ($scope.user_type == "rsm admin" || $scope.user_type == "schneider admin") {
        $scope.invamntdisabled = true;
        $scope.invamntexclvatdisabled = true;

      }
    }


  });

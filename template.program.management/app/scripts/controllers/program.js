/**
 * Created by DRIPL_1 on 04/17/2015.
 */

angular.module('vstford')
    .controller('programCtrl', function ($scope , $state,  $http, $q,
                                         ClaimService, UtilService,$stateParams,$rootScope) {


        //  $rootScope.invoice_data;

        // New Addition
        $scope.pos_amount = 0;
        $scope.balance_amount = 0;


        //End

        $scope.status = $stateParams.status;
        $scope.comp_name_list = [];
        $scope.duplicate_pop_show=false;

        $scope.show_loader=false;

        var login_user_type=UtilService.get_user_type();

        if(login_user_type=="schneider admin"){

            if( $scope.status ===typeof undefined || $scope.status == null)
            {
                $scope.status = 'VST Verification Required';
            }
          //  $scope.status = 'VST Verification Required';
        }
        else if(login_user_type=="rsm admin"){

            if( $scope.status ===typeof undefined || $scope.status == null)
            {
                $scope.status = 'pending';
            }

        }
        else
        {
            if( $scope.status ===typeof undefined || $scope.status == null)
            {
               $scope.status = 'pending';
            }

        }


        init();

        $scope.$watch('status', function(){

            $scope.show_loader=true;
            get_invoce_by_status_comp_name();

        });
//        $scope.$watch('company_name', function(){
//
////            get_invoce_by_status_comp_name();
//
//        });

        $scope.pop_up_close = function () {
            $scope.duplicate_pop_show=false;
        }


        function init() {


//            UtilService.change_header($state.current.data.header_label);
//            UtilService.select_menu($state.current.name);
            $rootScope.title = $state.current.title;
            $scope.show_cond = 'h';
//            get_user_company_name();
            // get_all_company_name();
            get_invoce_by_status_comp_name();
             $scope.postxt = false;
            $scope.detailsbtn = false;
            $scope.tot_amount = 0;
            $scope.tot_bal_amount = 0;

//            get_retailer_list()
            //  get_region_level_wise();
            //  get_all_program();
        }

        $scope.get_autocomplete_values = function () {
            var search_comp_name = $scope.company_name;
            if (search_comp_name.length > 2) {
                get_user_company_name();
            }
        }

        $scope.get_autocomplete_values_new = function () {
            var search_comp_name = $scope.company_name;
            if (search_comp_name.length > 2) {
                get_user_company_name_new();
            }
        }


        function get_user_company_name(){

            var company=UtilService.get_company();
            var srch_key=$scope.company_name;
            UtilService.show_loader();
            ClaimService.get_user_company_name($http, $q, company, srch_key)
                .then(function (data) {
                    UtilService.hide_loader();
                    $scope.comp_name_list =[];
                    var comp_name_array=[];
                    angular.forEach(data, function(item) {
                        comp_name_array.push(item.company_name)
                    });
                    $scope.comp_name_list =comp_name_array;
//                    $scope.retailer_list = data;


                },
                function () {
                    UtilService.hide_loader();
                    //Display an error message
                    $scope.error = error;

                });



        }


        function get_user_company_name_new(){

            var company=UtilService.get_company();
            var srch_key=$scope.company_name;
            UtilService.show_loader();
            ClaimService.get_user_company_name_new($http, $q, company, srch_key)
                .then(function (data) {
                    UtilService.hide_loader();
                    $scope.comp_name_list_new =[];
                    var comp_name_array=[];
                    angular.forEach(data, function(item) {
                        comp_name_array.push(item)
                    });
                    $scope.comp_name_list_new =comp_name_array;
//                    $scope.retailer_list = data;


                },
                function () {
                    UtilService.hide_loader();
                    //Display an error message
                    $scope.error = error;

                });



        }


//        function get_retailer_list(){
//            UtilService.show_loader();
//            ClaimService.get_retailer_list($http, $q)
//                .then(function (data) {
//                    UtilService.hide_loader();
//                    $scope.distributor_list = data;
//
//                    if (data[0].message != "no result found") {
//                        $scope.not_end_of_list = true;
//                    }
//                    else {
//                        $scope.s_data = data;
//                        $scope.end_of_list = false;
//                    }
//                },
//                function () {
//                    UtilService.hide_loader();
//                    //Display an error message
//                    $scope.error = error;
//
//                });
//
//        }
//
//
//        $scope.get_autocomplete_values = function () {
//            var search_distributor = $scope.company_name;
//            if (search_distributor.length > 2) {
//                get_retailer_list();
//            }
//        }







//        function get_retailer_list(){
//            UtilService.show_loader();
//            ClaimService.get_retailer_list($http, $q)
//                .then(function (data) {
//                    UtilService.hide_loader();
//                    $scope.retailer_list = data;
//
//                    if (data[0].message != "no result found") {
//                        $scope.not_end_of_list = true;
//                    }
//                    else {
//                        $scope.s_data = data;
//                        $scope.end_of_list = false;
//                    }
//                },
//                function () {
//                    UtilService.hide_loader();
//                    //Display an error message
//                    $scope.error = error;
//
//                });
//
//        }

//        function get_all_company_name() {
//            UtilService.show_loader();
//            ClaimService.get_all_company_name($http, $q)
//                .then(function (data) {
//                    UtilService.hide_loader();
//                    $scope.company_list = data;
//                    if (data[0].message != "no result found") {
//                        $scope.not_end_of_list = true;
//                    }
//                    else {
//                        $scope.region_list = data;
//                        $scope.end_of_list = false;
//                    }
//
//                },
//                function () {
//                    UtilService.hide_loader();
//                    //Display an error message
//                    $scope.error = error;
//
//                });
//        }



        // $scope.user_name="";


        $scope.get_invoice_by_retailer_name = function () {
            UtilService.show_loader();
            ClaimService.get_invoice_by_retailer_name($http, $q, $scope.retailer_name)
                .then(function (data) {
                    UtilService.hide_loader();
                    $scope.invoice = data;

                    if (data[0].message != "no result found") {
                        $scope.not_end_of_list = true;
                    }
                    else {
                        $scope.region_list = data;
                        $scope.end_of_list = false;
                    }

                },
                function () {
                    UtilService.hide_loader();
                    //Display an error message
                    $scope.error = error;

                });

        }


        $scope.download_invoice = function (sup_doc) {
            window.open(sup_doc);
        }

        $scope.get_invoce_by_comp_name = function (sup_doc) {
            get_invoce_by_status_comp_name();
        }

        function get_invoce_by_status_comp_name () {

            var status=$scope.status;
            var company_name=$scope.company_name;
            if ($scope.company_name == "" || typeof $scope.company_name === 'undefined' ){
                company_name='undefined';
            }
            UtilService.show_loader();
            ClaimService.get_invoce_by_status_comp_name($http, $q, status, company_name)
                .then(function (data) {

                    UtilService.hide_loader();
                    $scope.invoice = data;
                    $scope.show_loader=false;

                },
                function () {
                    UtilService.hide_loader();
                    //Display an error message
                    $scope.error = error;

                });

        }


//        $scope.get_user_by_comp_name = function () {
//            ClaimService.get_user_by_comp_name($http, $q, $scope.sel_comp_name)
//                .then(function (data) {
//
//                    $scope.user_list = data;
//
//                    if (data[0].message != "no result found") {
//                        $scope.not_end_of_list = true;
//                    }
//                    else {
//                        $scope.region_list = data;
//                        $scope.end_of_list = false;
//                    }
//
//                },
//                function () {
//                    //Display an error message
//                    $scope.error = error;
//
//                });
//
//        }

        $scope.invoice_details_by_number = function(claim_id,user_id )
        {
            $state.go( "main.program_details", {'claim_id' : claim_id,'user_id':user_id} );
        }


        $scope.get_duplicate_invoice_list = function (invoice_number) {


            UtilService.show_loader();
            ClaimService.get_duplicate_invoice_list($http, $q, invoice_number)
                .then(function (data) {
                    UtilService.hide_loader();
                    if(data.length==0 || data.length==1 ){

                        alert('No Duplicate Invoice Found ');

                    }
                    else
                    {

                        $scope.duplicate_invoice_list = data;
                        $scope.duplicate_pop_show=true;
                    }

                },
                function () {
                    UtilService.hide_loader();
                    //Display an error message
                    $scope.error = error;

                });

        }

        $scope.save_sku = function()
        {
            UtilService.show_loader();
            ClaimService.save_sku($http, $q, $scope.sku_data)
                .then(function (data) {
                    UtilService.hide_loader();
                    $scope.show_cond='s';
                    alert(data);

                },
                function () {
                    //Display an error message
                    UtilService.hide_loader();
                    $scope.error = error;

                });
        }

        // New Addition (Upload VS POS)

        $scope.get_si_details = function()
        {
            var company_data = _.where($scope.comp_name_list_new,{company_name:$scope.company_name });
            if(company_data.length > 0)
            {
            var status = company_data[0].current_status;
            var previous_status = company_data[0].previous_status_name;
            $scope.previous_status = previous_status;
            if(status == 1)
            {
                $scope.POSxSEEP = true;
                $scope.POSxNONSEEP = false;
                $scope.POSnSEEP = false;
                $scope.POSnNONSEEP = false;
                $scope.ENTx = false;
                $scope.ENTn = false;
                $scope.Only_SEEP = false;

            }
            else if(status == 2)
            {

                $scope.POSxSEEP = false;
                $scope.POSxNONSEEP = true;
                $scope.POSnSEEP = false;
                $scope.POSnNONSEEP = false;
                $scope.ENTx = false;
                $scope.ENTn = false;
                $scope.Only_SEEP = false;

            }
            else if(status == 3)
            {
                $scope.POSxSEEP = false;
                $scope.POSxNONSEEP = false;
                $scope.POSnSEEP = true;
                $scope.POSnNONSEEP = false;
                $scope.ENTx = false;
                $scope.ENTn = false;
                $scope.Only_SEEP = false;
            }
            else if(status == 4)
            {
                $scope.POSxSEEP = false;
                $scope.POSxNONSEEP = false;
                $scope.POSnSEEP = false;
                $scope.POSnNONSEEP = true;
                $scope.ENTx = false;
                $scope.ENTn = false;
                $scope.Only_SEEP = false;

            }
            else if(status == 5)
            {
                $scope.POSxSEEP = false;
                $scope.POSxNONSEEP = false;
                $scope.POSnSEEP = false;
                $scope.POSnNONSEEP = false;
                $scope.ENTx = true;
                $scope.ENTn = false;
                $scope.Only_SEEP = false;

            }
            else if(status == 6)
            {
                $scope.POSxSEEP = false;
                $scope.POSxNONSEEP = false;
                $scope.POSnSEEP = false;
                $scope.POSnNONSEEP = false;
                $scope.ENTx = false;
                $scope.ENTn = true;
                $scope.Only_SEEP = false;

            }
            else
            {
                $scope.POSxSEEP = false;
                $scope.POSxNONSEEP = false;
                $scope.POSnSEEP = false;
                $scope.POSnNONSEEP = false;
                $scope.ENTx = false;
                $scope.ENTn = false;
                $scope.Only_SEEP = true;
            }
            }
        }


        $scope.get_details = function()
        {
            UtilService.show_loader();

            if($scope.month == "cumulative")
            {
                var ret_data = {retailer:$scope.company_name};
                ClaimService.get_all_data($http, $q,ret_data)
                    .then(function (data) {
                        UtilService.hide_loader();
                        var final_arr = [];
                        var final_arr_obj = {};
                        $scope.tot_amount = 0;
                        $scope.tot_bal_amount = 0;
                       if(data.length > 0){
                       for(var i=0;i<data[0].pos_details.length;i++)
                       {
                           final_arr_obj = {uploaded_month:data[0].pos_details[i].month,total_sale:data[0].pos_details[i].invoice_amount_excl_vat,pos_amount:data[0].pos_details[i].pos,balance_amount:data[0].pos_details[i].balance_amount};
                           final_arr.push(final_arr_obj);
                           $scope.tot_amount = $scope.tot_amount + data[0].pos_details[i].invoice_amount_excl_vat;
                           $scope.tot_bal_amount = $scope.tot_bal_amount + data[0].pos_details[i].balance_amount;
                       }

                       }

                        $scope.uploaded_data = final_arr;
                        $scope.detailsbtn = true;
                        $scope.postxt = true;
                        // alert(data);

                    },
                    function () {
                        //Display an error message
                        UtilService.hide_loader();
                        $scope.error = error;

                    });



            }
            else
            {

                $scope.detailsbtn = false;
                $scope.postxt = false;
               var dataobj = {retailer:$scope.company_name,month:$scope.month};

                ClaimService.get_invoice_amount($http, $q,dataobj)
                .then(function (data) {
                    UtilService.hide_loader();

                    $scope.uploaded_data = data;
                    $scope.tot_amount = $scope.uploaded_data[0].total_sale;
                    $scope.tot_bal_amount = $scope.uploaded_data[0].balance_amount;

                   // alert(data);

                },
                function () {
                    //Display an error message
                    UtilService.hide_loader();
                    $scope.error = error;

                });

            }

        }


        $scope.get_balance_amount = function()
        {
            $scope.uploaded_data[0].balance_amount = ($scope.uploaded_data[0].total_sale - $scope.uploaded_data[0].pos_amount);
            $scope.tot_bal_amount = $scope.uploaded_data[0].balance_amount;

        }

        $scope.insert_pos_data = function()
        {
            var pos_data = {};
            var amount_arr = [];
            var amount_data = {};
            amount_data.month = $scope.month;
            amount_data.invoice_amount_excl_vat = $scope.uploaded_data[0].total_sale;
            amount_data.pos = $scope.uploaded_data[0].pos_amount;
            amount_data.balance_amount = $scope.uploaded_data[0].balance_amount;
            amount_arr.push(amount_data);
            pos_data.created_by = UtilService.get_from_localstorage('user_info').email_id;
            pos_data.retailer = $scope.company_name;
            pos_data.pos_details = amount_arr;


            ClaimService.insert_pos_details($http, $q,pos_data )
                .then(function(){

                    alert('POS details has been updated');

                })

        }

    });

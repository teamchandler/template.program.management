/**
 * Created by hassan on 15-04-2016.
 */


/**
 * Created by DRIPL_1 on 04/21/2015.
 */


'use strict';

angular.module('vstford')
  .controller('CompanyCtrl', function ($scope, $state,$http, $q,
                                    NotificationService, UtilService , ClaimService,$rootScope ) {


    var redeem=0;
    var acroot_point = 0;
    $scope.remain_points = true;
    $scope.data_count = "";
    /* var d = new Date();
     var year = d.getFullYear();
     var month = d.getMonth();
     var startDate = year + "-" + month + "-01";
     var endDate = year + "-" + month + "-29";*/

    /*var startDate = "11/01/2014";
     var endDate = "11/30/2014";
     */

    var init = function()
    {
      $rootScope.title = $state.current.title;

      // For Hiding Dashboard in Home Page

      $scope.ishide = false;
      $scope.ishidenew = true;
      $scope.ishidetab = false;
      $scope.ishidetab1 = true;
      // UtilService.change_header($state.current.data.header_label);
//            UtilService.select_menu($state.current.name);

//            $scope.exampleData =  {
//                "title": "Revenue",
//                "subtitle": "US$, in thousands",
//                "ranges": [150, 225, 300],
//                "measures": [220],
//                "markers": [250]
//            };

      $scope.user = UtilService.get_from_localstorage("user_info");
      if($scope.user.credit_point_normal == null)
      {
        $scope.user.credit_point_normal  = 0;
      }
      else if($scope.user.credit_point_special == null)
      {
        $scope.user.credit_point_special  = 0;

      }

      $scope.value = 0;
      $scope.upperLimit = 500;
      $scope.lowerLimit = 0;
      $scope.unit = "%";
      $scope.precision = 2;
      $scope.ranges = [
        {
          min: 0,
          max: 100,
          color: '#DEDEDE'
        },
        {
          min: 100,
          max: 200,
          color: '#8DCA2F'
        },
        {
          min: 200,
          max: 300,
          color: '#FDC702'
        },
        {
          min: 300,
          max: 400,
          color: '#FF7700'
        },
        {
          min: 400,
          max: 500,
          color: '#C50200'
        }
      ];



      get_points_by_user_id($scope.user);


      get_total_sale_of_user($scope.user);



      get_top_si();


      get_top_si_by_region();

      //get_user_details_for_zone_address($scope.user);

      get_user_goal_value($scope.user.email_id);

      get_user_redeemed_points($scope.user.email_id);

      get_user_proceedings_invoice_current_month($scope.user);

      get_user_si_region_dashboard($scope.user);
      get_user_si_allIndia_dashboard($scope.user);

      check_user_type();
      //console.log($scope.user);


      //  $scope.value = 5;

    }


    $scope.numeric_only = function (event) {

//            alert(event);
//            alert(event.keyCode);
      // Allow only backspace and delete
      if (event.keyCode == 46 || event.keyCode == 8 ||event.keyCode ==190 ) {
        // let it happen, don't do anything
      }
      else {
        // Ensure that it is a number and stop the keypress
        if (event.keyCode < 48 || event.keyCode > 57) {
          event.preventDefault();

        }
      }
    };



    function check_user_type()
    {
      if(user_type == "rsm admin")
      {
        $scope.ishide = true;
        $scope.ishidenew = false;
        $scope.ishidetab = true;
        $scope.ishidetab1 = false ;

      }

    }


    function get_value() {
      if ($scope.points_till_date != null || typeof $scope.points_till_date !== 'undefined' && $scope.max != null || typeof $scope.max !== 'undefined') {
        //alert(Math.round(parseInt(100*($scope.points_till_date*1))/$scope.max));
        // $scope.value = 5;
        $scope.value = ($scope.points_till_date * 100) / $scope.max;
        // alert($scope.value);
        // var floatValue = parseFloat($scope.value);
//                if (!isNaN(parseFloat($scope.value))) {
//                    //float goes here
//
//                  //  $scope.value = 0;
//
//                }

      }
      else
      {
        $scope.value = 0;

      }

    }
    $scope.submit_Goal = function () {
      $scope.user = UtilService.get_from_localstorage("user_info");
      var goalValue = $scope.goal;
      $scope.user = $scope.user.email_id;
      $scope.user_id = $scope.user;
      if (goalValue == '' || typeof goalValue == "undefined") {
        /** Showing Error Message**/
        $scope.errorTextAlert = "Value Cannot be Empty";
        $scope.showErrorAlert = true;

        // switch flag
        $scope.switchBool = function (value) {
          $scope[value] = !$scope[value];
        }
        /**End Of Error Message**/

        /** Redirecting to Same Page for Reload
         * state.go() was not successfull
         * init() was used rather
         */
          //$state.go("main.home");
        init();

      }
      else {

        if ($scope.data_count > 0) {
          ClaimService.updating_goal_value($http, $q, goalValue, $scope.user)
            .then(function (data) {
                //console.log(data);
                $scope.topperNational = data;
                $scope.numLimit = 4;
                //alert("Goal Updated Successfully");

                /** Showing Success Message**/
                $scope.successTextAlert = "Goal Updated Successfully";
                $scope.showSuccessAlert = true;

                // switch flag
                $scope.switchBool = function (value) {
                  $scope[value] = !$scope[value];
                }
                /**End Of Success Message**/

                /** Redirecting to Same Page for Reload
                 * state.go() was not successfull
                 * init() was used rather
                 */
                  //$state.go("main.home");
                init();

              },
              function () {
                $scope.error = error;
              });

        }
        else {

          ClaimService.posting_goal_value($http, $q, goalValue, $scope.user)
            .then(function (data) {
                //console.log(data);
                $scope.topperNational = data;
                $scope.numLimit = 4;
                /** Showing Success Message**/
                $scope.successTextAlert = "Goal Set Successfully";
                $scope.showSuccessAlert = true;

                // switch flag
                $scope.switchBool = function (value) {
                  $scope[value] = !$scope[value];
                }
                /**End Of Success Message**/
                init();
              },
              function () {
                $scope.error = error;
              });
        }
      }
    }


    init();


    function get_top_si() {
      ClaimService.get_top_si($http, $q)
        .then(function (data) {
            //console.log(data);
            //var counter = 0;
            $scope.topperNational = data;
            $scope.numLimit = 4;
          },
          function () {
            $scope.error = error;
          });
    }


    function get_top_si_by_region() {
      ClaimService.get_top_si_by_region($http, $q)
        .then(function (data) {
            //console.log(data);
            $scope.topperRegion = data;
            $scope.numLimit = 4;
          },
          function () {
            $scope.error = error;
          });
    }


    function get_user_si_region_dashboard(user) {
      $scope.regionName = user.schneider_region;
      //console.log($scope.regionName);
      ClaimService.get_user_si_region_dashboard($http, $q, $scope.regionName)
        .then(function (data) {
            //console.log(data);
            $scope.totalRegionSi = data.length;

            for (var i = 0; i < data.length; i++) {
              if (user.email_id == data[i].user_id) {
                var indexer = i;
                $scope.rankIndex = parseInt(indexer) + 1;

              }

            }
          },
          function () {
            $scope.error = error;
          });
    }

    function get_user_si_allIndia_dashboard(user) {
      ClaimService.get_user_si_allIndia_dashboard($http, $q)
        .then(function (data) {
            //console.log(data);
            $scope.totalIndiaSi = data.length;

            for (var i = 0; i < data.length; i++) {
              if (user.email_id == data[i].user_id) {
                var indexer = i;
                $scope.rankIndexIndia = parseInt(indexer) + 1;

              }

            }

          },
          function () {
            $scope.error = error;
          });
    }


    function get_user_details_for_zone_address(user) {

      $scope.user_id = user.email_id;
      ClaimService.get_user_details_for_zone_address($http, $q, $scope.user_id)
        .then(function (data) {
            //console.log(data);
            $scope.userAddressZone = data;
          },
          function () {
            $scope.error = error;
          });
    }


    function get_user_redeemed_points(user) {
      ClaimService.get_user_redeemed_points($http, $q, user)
        .then(function (data) {
            //console.log(data);
            if (data[0].redeemed != null) {
              $scope.redeemedPoints = data[0].redeemed;
              redeem= parseInt(data[0].redeemed);
              $scope.current_point_balance=(acroot_point - redeem );
            }
            else {
              $scope.redeemedPoints = 0;
              redeem=0;
            }
          },
          function () {
            $scope.error = error;
          });
    }


    function get_user_goal_value(user) {
      var hs = '';
      // $scope.user_id = user.email_id;
      ClaimService.get_user_goal_value($http, $q, user)
        .then(function (data) {
            //console.log(data);

            if (data.length > 0) {
              $scope.max = data[0].goal;
            }
            else {
              $scope.max = 4000;
            }

            get_value();

            //alert($scope.points_till_date);
            //$scope.dynamic = value;


            $scope.data_count = data.length;


          },
          function () {
            $scope.error = error;
          });

      //$scope.valueguage = 20;


    }


    //function get_total_sale_of_user defination

    /**get_details_total_sale_by_user function  gets called in services ClaimService
     *
     * @param user passed from function definition
     * @param user_id passed in function to get particular user's details from function
     */

    function get_total_sale_of_user(user) {
      $scope.user_id = user.email_id;
      ClaimService.get_details_total_sale_by_user($http, $q, $scope.user_id)
        .then(function (data) {

            var sum = 0;
            if (data.length != 0) {
              $scope.rules = UtilService.get_program_rules();
              $scope.all_rules = $scope.rules[0].rules;
              //console.log(data); /** Displaying the data in console log of browser **/
              for (var i = 0; i < data.length; i++) {
                //   sum = sum + data[i].total_sale;
                if(data[i].status == "Verified & Approved")
                {
                  $scope.ttl_sale_value =  data[i].total_sale;

                }
              }
              //    $scope.totalSale = sum;


//                            $scope.exampleData = [
//                             { key: "One", y: 5 },
//                                     { key: "Two", y: 2 },
//                                     { key: "Three", y: 9 },
//                                    { key: "Four", y: 7 },
//                                     { key: "Five", y: 4 },
//                                     { key: "Six", y: 3 },
//                                     { key: "Seven", y: 9 }];


              $scope.exampleData = [];

              for (var j = 0; j < data.length; j++) {

                var k = {
                  key: data[j].status,
                  y: data[j].total_sale

                };
                $scope.exampleData.push(k);
              }

              //console.log($scope.exampleData);




              $scope.width = 500;
              $scope.height = 500;
              $scope.xFunction = function () {
                return function (d) {
                  return d.key;
                };
              }
              $scope.yFunction = function () {
                return function (d) {
                  return d.y;
                };
              }

              if (data.length == 3) {
                var colorArray = ['#e6c26e', '#5CA638', '#E80F0F'];
                $scope.colorFunction = function () {
                  return function (k, i) {
                    return colorArray[i];
                  };
                }
              }
              if (data.length < 3) {
                if ($scope.exampleData[0].key == 'Rejected') {
                  var colorArray = ['#E80F0F', '#5CA638'];
                  $scope.colorFunction = function () {
                    return function (k, i) {
                      return colorArray[i];
                    };
                  }
                }
              }

              if (data.length < 3) {
                if ($scope.exampleData[0].key == 'Verified & Approved') {
                  var colorArray = ['#5CA638', '#E80F0F'];
                  $scope.colorFunction = function () {
                    return function (k, i) {
                      return colorArray[i];
                    };
                  }
                }
              }

              if (data.length < 3) {
                if ($scope.exampleData[0].key == 'VST Verification Required') {
                  if ($scope.exampleData[1].key == 'Verified & Approved') {
                    var colorArray = ['#e6c26e', '#5CA638'];
                    $scope.colorFunction = function () {
                      return function (k, i) {
                        return colorArray[i];
                      };
                    }
                  }
                }
              }

              $scope.descriptionFunction = function () {
                return function (d) {
                  return d.key;
                }
              }

            }

          },
          function () {
            //Display an error message
            $scope.error = error;

          });
    }


    function get_user_proceedings_invoice_current_month(user) {
      $scope.user_id = user.email_id;


      var current_date = new Date();  // Current date
      current_date.setDate(current_date.getDate()-30) // Subtract 30 days

      ClaimService.get_user_proceedings_invoice_current_month($http, $q, $scope.user_id,current_date )
        .then(function (data) {
            //console.log(data);
            $scope.allData = data;
            $scope.dataCount = data.length;

          },
          function () {
            //Display an error message
            $scope.error = error;

          });
    }


    /**End of Subhajit Edit**/


    function get_points_by_user_id(user) {
      $scope.user_id = user.email_id;

      ClaimService.get_details_by_user($http, $q, $scope.user_id)
        .then(function (data) {


            var special = 0;
            var normal = 0;
            var bonus = 0;
            var remainder = 0;

            if (data.length != 0) {

//                            $scope.rules = UtilService.get_program_rules();
//                            $scope.all_rules = $scope.rules[0].rules;


              for (var j = 0; j < data.length; j++) {

                normal = Math.round(data[j].total_sale);

              }

              $scope.total_amount = Math.round(( normal) / 1000);

              var slab_point=0;
              var total_amt=$scope.total_amount;
//                            if(total_amt>=250 && total_amt<500)
//                            {
//                                slab_point=250;
//                            }
//                            else if(total_amt>=500 && total_amt<1000)
//                            {
//                                slab_point=500;
//                            }
//                            else if(total_amt>=1000 && total_amt<1500)
//                            {
//                                slab_point=1000;
//                            }
//                            else if(total_amt>=1500 && total_amt<2000)
//                            {
//                                slab_point=1500;
//                            }
//                            else if(total_amt>=2000 && total_amt<3000)
//                            {
//                                slab_point=2000;
//                            }
//                            else if(total_amt>=3000 && total_amt<4000)
//                            {
//                                slab_point=3000;
//                            }
//                            else if(total_amt>=4000)
//                            {
//                                slab_point=4000;
//                            }

              //Added By Mahiruddin Seikh for Next Slap point Calculation on date:21/08/2015


              if(total_amt>=250 && total_amt<500)
              {
                slab_point=500;
              }
              else if(total_amt>=500 && total_amt<1000)
              {
                slab_point=1000;
              }
              else if(total_amt>=1000 && total_amt<1500)
              {
                slab_point=1500;
              }
              else if(total_amt>=1500 && total_amt<2000)
              {
                slab_point=2000;
              }
              else if(total_amt>=2000 && total_amt<3000)
              {
                slab_point=3000;
              }
              else if(total_amt>=3000 && total_amt<4000)
              {
                slab_point=4000;
              }
              else if(total_amt>=4000)
              {
                slab_point=4000;
              }

              acroot_point = Math.round($scope.total_amount);
              $scope.points_till_date = acroot_point;
              remainder = (acroot_point % slab_point);
              $scope.remainder = remainder;

              //Added By Mahiruddin Seikh for Next Slab point Calculation on date:21/08/2015

              $scope.current_point_balance=(acroot_point-redeem);

              var is_NaN  = isNaN(remainder);

              if( is_NaN == true)
              {
                remainder = 0;

              }

              $scope.next_slab = Math.round(slab_point - remainder);

              $scope.current_point = Math.round((acroot_point % slab_point) + user.total_point * 1);

//                            var sp = 0;
//
//                            if($scope.current_point>=0 && $scope.current_point<249)
//                            {
//                                sp=250;
//                            }
//                            else if($scope.current_point>=250 && $scope.current_point<500)
//                            {
//                                sp=500;
//                            }
//                            else if($scope.current_point>=500 && $scope.current_point<1000)
//                            {
//                                sp=1000;
//                            }
//                            else if($scope.current_point>=1000 && $scope.current_point<1500)
//                            {
//                                sp=1500;
//                            }
//                            else if($scope.current_point>=1500 && $scope.current_point<2000)
//                            {
//                                sp=2000;
//                            }
//                            else if($scope.current_point>=2000 && $scope.current_point<3000)
//                            {
//                                sp=3000;
//                            }
//                            else if($scope.current_point>=3000 && $scope.current_point<4000)
//                            {
//                                sp=4000;
//                            }
//
//
//                            $scope.next_slab = Math.round(sp -$scope.current_point);

              if ($scope.current_point < 0) {
                $scope.current_point = 0;

              }
              else if ($scope.current_point > 4000) {

                $scope.remain_points = false;

              }



//                    $scope.credit_point_special=(user.credit_point_special * 1).toFixed(0);
//                  //  $scope.total_amount = data[0].total_sale;
//                    $scope.normal_point =  Math.round($scope.total_amount / 1000);
//
//                    $scope.total_point=(($scope.normal_point * 1 - user.credit_point_normal * 1) + (user.total_point * 1)).toFixed(0);
//
//
//                    $scope.special_points = $scope.user.credit_point_special  * 1  ;
//                    var pt =  ($scope.normal_point + $scope.special_points * 1 ) ;
//                    $scope.total_points = pt;
//
//
//                    var j =  $scope.all_rules.length - 1;
//
//                    if ($scope.normal_point >  $scope.all_rules[j].award )
//                        {
//                            $scope.remain_points = false;
//
//                        }


//                    for(var i=0;i< $scope.all_rules.length ; i++)
//                      {
//                          $scope.award = $scope.all_rules[i].award;
//
//                          if($scope.normal_point <= $scope.award )
//                          {
//
//                              if(($scope.normal_point*1)==($scope.award*1))
//                              {
//                                  $scope.remaining_points = 500;
//                              }
//                              else
//                              {
//                                  $scope.remaining_points = ($scope.award - $scope.normal_point) * 1;
//                              }
//
//
//                          return false;
//
//                          }
//
//                      }

            }

            else {

              $scope.points_till_date = Math.round(0);
              // $scope.total_points = $scope.total_points + ($scope.user.credit_point_special  * 1);
              $scope.next_slab = 0;

              $scope.current_point = 0;
              $scope.total_amount = 0;
              $scope.bonus = 0;

            }


          },
          function () {
            //Display an error message
            $scope.error = error;

          });


    }

  });


'use strict';

/**
 * @ngdoc function
 * @name vstford.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the vstford
 */
angular.module('vstford')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

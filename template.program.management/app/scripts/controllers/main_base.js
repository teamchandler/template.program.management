'use strict';

angular.module('vstford')
  .controller('MainBaseCtrl', function ($scope, $state ,UtilService,$rootScope
                                       )
    {



        $scope.$on('admin_login', function () {

            $scope.prog_admin = true;
            $scope.claim_entry = true;    //Atul 16 april
            $scope.redemption = false;
            $scope.my_claims = false;
            $scope.reward = false;
            $scope.payment_upload = true;
            $scope.payment = true;
            $scope.claim_entry_upload = true;

        });


        $scope.$on('user_login', function () {

            $scope.prog_admin = false;
            $scope.claim_entry = true;
            $scope.redemption = true;
            $scope.my_claims = true;
            $scope.reward = true;

          //  $state.go("main.home");
        });

        $scope.$on('rsm_login', function () {

            $scope.prog_admin = true;
            $scope.claim_entry = false;
            $scope.redemption = false;
            $scope.my_claims = false;
            $scope.reward = true;
            //  $state.go("main.home");
        });



        var check_user = function(){
            var u = UtilService.get_from_localstorage('user_info');
            //console.log(u);
            if ((u == "") || (u == null)) {
                // send to login
                $state.go('login.login');

            }
        }

        var init = function(){
            $rootScope.title = $state.current.title;
           check_user();
            $scope.type_of_user = "";
            $scope.prog_admin = false;
//            $scope.claim_entry = false;
//            $scope.redemption = false;

            var u = UtilService.get_from_localstorage("user_info");


            if(u!=null) {
                for (var i = 0; i < annectos_program_admin.length; i++) {
                    if (u.email_id != null && u.email_id != "") {
                        if (u.email_id.toLowerCase() == annectos_program_admin[i]) {

                          //  $scope.type_of_user = "annectos admin";
                            user_type = "annectos admin";
                            $rootScope.$broadcast('admin_login');

                        }
                    }

                }

                for (i = 0; i < schneider_program_admin.length; i++) {
                    if (u.email_id != null && u.email_id != "") {
                        if (u.email_id.toLowerCase() == schneider_program_admin[i]) {

                           // $scope.type_of_user = "schneider admin";
                            user_type = "schneider admin";
                            $rootScope.$broadcast('admin_login');

                            break;
                        }
                    }
                }

                // Newly added for new user type

                for (i = 0; i < rsm_admin.length; i++) {
                    if (u.email_id != null && u.email_id != "") {
                        if (u.email_id.toLowerCase() == rsm_admin[i]) {

                            // $scope.type_of_user = "schneider admin";
                            user_type = "rsm admin";
                            $rootScope.$broadcast('rsm_login');

                            break;
                        }
                    }
                }


            }


//            if($scope.type_of_user == "" || typeof $scope.type_of_user === 'undefined')
//            {
//
//                $rootScope.$broadcast('user_login');
//            }

            if(user_type=="schneider user")
            {
                $rootScope.$broadcast('user_login');

            }
        }
        init();

  });

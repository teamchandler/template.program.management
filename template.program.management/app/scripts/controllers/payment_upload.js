/**
 * Created by dev11 on 1/28/2016.
 */

'use strict';

angular.module('vstford')
    .controller('PaymentUploadCtrl',function ($scope,$http,$q,UtilService,PaymentService,$window) {

        $scope.payment_arry=[];

        $scope.showContent = function($fileContent) {
            UtilService.show_loader();
            $scope.content = $fileContent;


            var full_csv_data = [];
            var str = $scope.content;
            var column_data = [];
            var myarray = str.split(/\n/g);
            myarray.splice((myarray.length - 1), 1);

            if($scope.payment_arry.length>0){

                $scope.payment_arry=[];
                putting_data_in_grid();

            }
            else
            {
                putting_data_in_grid();
            }


            function putting_data_in_grid() {
                UtilService.show_loader();

                for (var i = 0; i < myarray.length; i++) {
                    var csv_obj = {};

                    var string = myarray[i];
                    column_data = string.split(';');
                    if (i != 0) {

                        csv_obj = {invoice_number: column_data[0], retailer_name: column_data[1], payment_date: column_data[2], amount_received: column_data[3], payment_received: column_data[4]};

                        $scope.payment_arry.push(csv_obj);
                    }


                }

                for (var j = 0; j < $scope.payment_arry.length; j++) {
                    var data = $scope.payment_arry[j].invoice_number;
                    var data2 = PaymentService.get_data_by_invoice_numb(data);
                    if (data2.err) {
                        UtilService.hide_loader();
                       // console.log(data2.err);
                        alert("Data Error");
                    }
                    else {
                        if (data2.length > 0) {
                            $scope.payment_arry[j].exist_in_db = "Y";
                        }
                        else {
                            $scope.payment_arry[j].exist_in_db = "N";
                        }
                        UtilService.hide_loader();
                       // console.log(data2);
                    }
                }
                UtilService.hide_loader();
            }


        };


        $scope.put_data_in_database = function () {


                for (var i = 0; i < $scope.payment_arry.length; i++) {
                    var data = $scope.payment_arry[i].invoice_number;
                    var data1 = PaymentService.add_payment_upload_from_csv(data);
                    if (data1.err) {
                        UtilService.hide_loader();
                        alert("Data Error");
                    }
                    else {


                    }
                }

                alert("Data Inserted");
            }

        var init = function(){

//            UtilService.show_loader();
//            UtilService.hide_loader();
        }

        init();

    });


'use strict';

angular.module('vstford')
  .controller('myprofileCtrl',function ($scope,$http,$q,UtilService,ProfileService,$rootScope,$state,$parse) {
        var user_data = UtilService.get_from_localstorage("user_info");

        var i=0;
        var j=0;
        var brands = "";
        var brands2 = "";
        $scope.custom = true;
        $scope.custom_data = true;
        $scope.non_custom_data = true;
//        var schender_data="";

    //added by Mahiruddin on date 21/05/2015
        // Item List Arrays
        $scope.items = [];
        $scope.nonitems = [];
        var itm = [];
        $scope.itms=[];

        //added on date 27/05/2015
//        var schender_data=[];
        var non_itm = [];
        $scope.non_itms=[];

        // Add a Item to the list
        $scope.addItem = function () {

            if ($scope.profile_data.schneider_brands == "" || typeof $scope.profile_data.schneider_brands === 'undefined' ){
                alert ("Add Schneider Brands");
                return false;
            }

            if($scope.profile_data.schneider_brands!=null)
            {
                $scope.custom_data = false;

            }

            itm=$scope.itms;
            itm.push({
                id:($scope.itms.length+1),
                schneider_brands_push: $scope.profile_data.schneider_brands
            });

            $scope.itms=itm;
            console.log(brands);
            $scope.profile_data.schneider_brands="";
        };

        //delete from list
        $scope.delete_item = function (index) {
//            $scope.itms.splice(index, 1);
            $scope.itms.splice(index, 1);
            i=0;
        };

        //added for edit item from list
        $scope.edit_item = function ($index,item) {
            i=item.id * 1;
            $scope.profile_data.schneider_brands=item.schneider_brands_push;
            $scope.delete_item($index);
        };

        //Section for non schneider_brands
        $scope.addItem2 = function () {

            if ($scope.profile_data.non_schneider_brands == "" || typeof $scope.profile_data.non_schneider_brands === 'undefined') {
                alert("Add Non Schneider Brands");
                return false;
            }

            if ($scope.profile_data.non_schneider_brands != null) {
                $scope.non_custom_data = false;

            }

            non_itm = $scope.non_itms;
            non_itm.push({
                nonid: ($scope.non_itms.length + 1),
                non_schneider_brands_push: $scope.profile_data.non_schneider_brands
            });

            $scope.non_itms = non_itm;

//            Clear input fields after push
            $scope.profile_data.non_schneider_brands = "";
        };


//        $scope.addItem2 = function () {
//
//            if ($scope.profile_data.non_schneider_brands == "" || typeof $scope.profile_data.non_schneider_brands === 'undefined' ){
//                alert ("Add Non Schneider Brands");
//                return false;
//            }
//
//            if($scope.profile_data.non_schneider_brands!=null)
//            {
//                $scope.non_custom_data = false;
//
//            }
//            $scope.non_itms=non_itm;
//
//            // Clear input fields after push
//            $scope.non_schneider_brands = "";
//        };

        $scope.non_edit_item = function ($index,item) {
            j=item.nonid * 1;
            $scope.profile_data.non_schneider_brands=item.non_schneider_brands_push;
            $scope.non_delete_item($index);
        };

        $scope.non_delete_item = function (index) {
//            $scope.nonitems.splice(index, 1);
            $scope.non_itms.splice(index, 1);
            j=0;
        };
        //add end on 21/05/2015

        init();

        function init()
        {
            $rootScope.title = $state.current.title;
            $scope.profile_data={};
            $scope.profile_address={};
            get_profile_info();
        };

        //get user basic info
        function get_profile_info(){
            UtilService.show_loader();
            ProfileService.get_profile_info($http, $q, user_data.email_id)
                .then
            (
                function (data) {

                    $scope.custom_data = false;
                    $scope.non_custom_data = false;
                    UtilService.hide_loader();
                    console.log(data[0][0]);
//                    $parse(expression);
                    $scope.profile_data=data[0][0];
                    var dta=data[0][0];
                    console.log(dta);

                    if(dta.schneider_brands!= "" || dta.schneider_brands!= "null"){
                        var pop_itm=JSON.parse(dta.schneider_brands);
                        if(pop_itm.length>0) {
                            if (pop_itm.length == 1) {
                                $scope.profile_data.schneider_brands = pop_itm[0].schneider_brands_push;
                            } else {
                                $scope.profile_data.schneider_brands="";
                                $scope.itms = pop_itm;
                            }
                        }

                    }

                    if(dta.non_schneider_brands!= "" || dta.non_schneider_brands!= "null"){
                        var non_pop_itm=JSON.parse(dta.non_schneider_brands);
                        if(non_pop_itm.length>0) {
                            if (non_pop_itm.length == 1) {
                                $scope.profile_data.non_schneider_brands = non_pop_itm[0].non_schneider_brands_push;
                            }
                            else {
                                $scope.profile_data.non_schneider_brands="";
                                $scope.non_itms = non_pop_itm;
                            }
                        }

                    }

//                    non_itm=JSON.parse(dta.non_schneider_brands)
//                    console.log(dta.schneider_brands);
//                    if(dta.non_schneider_brands!= ""){
//                        $scope.non_itms=JSON.parse(dta.non_schneider_brands);
//                    }
//                    $scope.itms=JSON.parse(dta.schneider_brands);
//                    if($scope.itms!= ""){
//
//                            $scope.profile_data.schneider_brands = null;
//                        }
//
//
//                    $scope.non_itms=JSON.parse(dta.non_schneider_brands);
//                    if($scope.non_itms.length>0)
//                    {
//                        $scope.profile_data.non_schneider_brands=null;
//                    }
                    console.log($scope.non_itms);
                    console.log(itm);
                    console.log(non_itm);
//                    $scope.nonitems=non_schender_data;
                    $scope.user_shipping_list = data[0][0];

                    if(data[0][0].comp_found_date=="00-00-0000"){
                        $scope.profile_data.comp_found_date="";
                    }
                    else
                    {
                        $scope.profile_data.comp_found_date=data[0][0].comp_found_date;
                    }
                    if(data[0][0].user_dob=="00-00-0000"){
                        $scope.profile_data.dob="";
                    }
                    else{
                        $scope.profile_data.dob=data[0][0].user_dob;
                    }
                },

                function () {
                    UtilService.hide_loader();
                    //Display an error message
                    $scope.error = error;
                }
            );
        }

        //Save My Profile data
        $scope.update_profile_basic = function(){

            if ($scope.profile_data.first_name == "" || typeof $scope.profile_data.first_name === 'undefined' ){
                alert ("Please enter First Name");
                return false;
            }
            else if ($scope.profile_data.last_name == "" || typeof $scope.profile_data.last_name === 'undefined' ){
                alert ("Please enter Last Name");
                return false;
            }
            else if ($scope.profile_data.dob == "" || typeof $scope.profile_data.dob === 'undefined' ){
                alert ("Please enter date of birth");
                return false;
            }
            $scope.profile_data.email_id=user_data.email_id;
            var dob=$scope.profile_data.dob;

            if(dob!=null && dob!=""){
                var dob_date_array= dob.split('-');
                var dob_date_date =parseInt(dob_date_array[0]);
                var dob_date_month=parseInt(dob_date_array[1]);
                var dob_date_year=parseInt(dob_date_array[2]);
                $scope.profile_data.user_dob=dob_date_year+"-"+dob_date_month+"-"+dob_date_date;
            }
            else{
                $scope.profile_data.user_dob="";
            }

            var cfd=$scope.profile_data.comp_found_date;

            if(cfd!=null && cfd!=""){
                var cfd_date_array= cfd.split('-');
                var cfd_date_date =parseInt(cfd_date_array[0]);
                var cfd_date_month=parseInt(cfd_date_array[1]);
                var cfd_date_year=parseInt(cfd_date_array[2]);
                $scope.profile_data.comp_found_date=cfd_date_year+"-"+cfd_date_month+"-"+cfd_date_date;
            }
            else{
                $scope.profile_data.comp_found_date="";
            }
            $scope.profile_basic_data={};
            $scope.profile_basic_data.first_name=$scope.profile_data.first_name;
            $scope.profile_basic_data.last_name=$scope.profile_data.last_name;
            $scope.profile_basic_data.user_dob=$scope.profile_data.user_dob;
            $scope.profile_basic_data.mobile_no=$scope.profile_data.mobile_no;
            $scope.profile_basic_data.comp_found_date=$scope.profile_data.comp_found_date;
            $scope.profile_basic_data.qualification=$scope.profile_data.qualification;
            $scope.profile_basic_data.email_id=$scope.profile_data.email_id;
            $scope.profile_basic_data.gender=$scope.profile_data.gender;

//            added by Mahiruddin on date: 18.05.2015
            $scope.profile_basic_data.personal_interests=$scope.profile_data.personal_interests;
//            brands= angular.toJson($scope.items);
//            brands2=angular.toJson($scope.nonitems);
            itm=$scope.itms;
            if(itm.length>0)
            {
                $scope.profile_basic_data.schneider_brands = angular.toJson(itm);

            }
            else
            {
                if($scope.profile_data.schneider_brands != "" && typeof $scope.profile_data.schneider_brands !== 'undefined')
                {
                    itm.push({
                        id:($scope.itms.length+1),
                        schneider_brands_push: $scope.profile_data.schneider_brands
                    });
                    $scope.profile_basic_data.schneider_brands = angular.toJson(itm);
                }
                else
                {
                    $scope.profile_basic_data.schneider_brands = "";
                }
            }

            non_itm=$scope.non_itms;
            if(non_itm.length>0)
            {
                $scope.profile_basic_data.non_schneider_brands = angular.toJson(non_itm);

            }
            else
            {
                if($scope.profile_data.non_schneider_brands != "" && typeof $scope.profile_data.non_schneider_brands !== 'undefined')
                {
                    non_itm.push({
                        id:($scope.non_itms.length+1),
                        non_schneider_brands_push: $scope.profile_data.non_schneider_brands
                    });
                    $scope.profile_basic_data.non_schneider_brands = angular.toJson(non_itm);
                }
                else
                {
                    $scope.profile_basic_data.non_schneider_brands = "";
                }
            }

//            $scope.profile_basic_data.non_schneider_brands = $scope.profile_data.non_schneider_brands;
//            $scope.profile_basic_data.non_schneider_brands= brands2;

//            $scope.profile_basic_data.brands=brands;;

            UtilService.show_loader();
            ProfileService.profile_basic_update($http, $q, $scope.profile_basic_data)
                .then
            (
                function (data) {
                    console.log(data);
                    UtilService.hide_loader();
                    alert('Profile information updated successfully');
                },

                function () {
                    //Display an error message
                    UtilService.hide_loader();
                    $scope.error = error;
                }
            );

        }

        //Save user address data
        $scope.update_user_address = function () {

            $scope.profile_address.email_id=UtilService.get_from_localstorage('user_info').email_id;
            $scope.profile_address.gender=$scope.profile_data.gender;
            //  $scope.selected_address.mobile_no=$scope.user_data.mobile_no;
            $scope.profile_address.office_no="0000";
            $scope.profile_address.country="India";
            $scope.profile_address.company=UtilService.get_company();


            if ($scope.profile_address.first_name == "" || typeof $scope.profile_address.first_name === 'undefined' ){
                alert ("Please enter first name");
                return false;
            }
            else if ($scope.profile_address.last_name == "" || typeof $scope.profile_address.last_name === 'undefined'){
                alert ("Please enter last name");
                return false;
            }
            else if ($scope.profile_address.street == "" || typeof $scope.profile_address.street === 'undefined'){
                alert ("Please enter landmark");
                return false;
            }
            else if ($scope.profile_address.city == "" || typeof $scope.profile_address.city === 'undefined'){
                alert ("Please enter city");
                return false;
            }
            else if ($scope.profile_address.state == "" || typeof $scope.profile_address.state === 'undefined'){
                alert ("Please enter state");
                return false;
            }
            else if ($scope.profile_address.zipcode == "" || typeof $scope.profile_address.zipcode === 'undefined'){
                alert ("Please enter pincode");
                return false;
            }
            else if ($scope.profile_address.mobile_no == "" || typeof $scope.profile_address.mobile_no === 'undefined'){
                alert ("Please enter phone number");
                return false;
            }
            UtilService.show_loader();
            ProfileService.update_profile_address($http, $q, $scope.profile_address)
                .then(function (data) {
                    UtilService.hide_loader();
                    alert('Profile information updated successfully');
                    get_profile_info();
                },
                function () {
                    //Display an error message
                    UtilService.hide_loader();
                    $scope.error = error;

                });
        }

        //populate Saved address
        $scope.select_address = function (selected_address) {
            $scope.profile_address.first_name = selected_address.first_name;
            $scope.profile_address.last_name = selected_address.last_name;
            $scope.profile_address.street = selected_address.street;
            $scope.profile_address.address = selected_address.address;
            $scope.profile_address.city = selected_address.city;
            $scope.profile_address.state = selected_address.state;
            $scope.profile_address.pincode = selected_address.pincode;
            $scope.profile_address.mobile_no = selected_address.mobile_no;
            $scope.profile_address.zipcode = selected_address.zipcode;
            //   selected_address.email_id = localStorageService.get('user_info').email_id;
            //   selected_address.store = utilService.get_store().toLowerCase();
//            $scope.profile_address = selected_address;
        }
  });



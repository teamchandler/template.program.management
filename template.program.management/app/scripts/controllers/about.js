'use strict';

/**
 * @ngdoc function
 * @name vstford.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the vstford
 */
angular.module('vstford')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

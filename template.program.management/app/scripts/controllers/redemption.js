'use strict';

angular.module('vstford')
  .controller('RedemptionCtrl', function ($scope,$http,$q,UtilService,MasterService,$state,$rootScope) {
        var user_data = UtilService.get_from_localstorage("user_info");
        init();
        function init() {
            $rootScope.title = $state.current.title;
            get_redemption_details();
        }

        //Get redemption Details
        function get_redemption_details() {
            UtilService.show_loader();
            MasterService.get_redemption_details($http, $q, user_data.email_id).then(function (data) {
                    UtilService.hide_loader();
                    $scope.my_orders = data[0];

                },
                function () {
                    //Display an error message
                    UtilService.hide_loader();
                    $scope.error = error;
                    $scope.loading = false;
                });


        }

        //Click on view more
        $scope.goto_redemption_more=function(or) {

            $state.go("main.redemption_detail",{'order_id' : or.order_id,'status':or.payment_info,'order_date':or.create_ts,'points':or.total_amount});

        }

    });

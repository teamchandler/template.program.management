'use strict';

/**
 * @ngdoc overview
 * @name vstford
 * @description
 * # vstford
 *
 * Main module of the application.
 */

angular
  .module('vstford', [
    'ui.router',
    'LocalStorageModule',
    'angularFileUpload',
    'ui.bootstrap',
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ngRadialGauge',
    'nvd3ChartDirectives'
    //    'ngCsvImport',
    //    'hljs'
  ])

  .config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/login/login');

    $stateProvider
      .state('login', {
        url: "/login",
        abstract: true,
        templateUrl: "views/login_base.html",
        controller: "LoginBaseCtrl"
      })
      .state('login.login', {
        url: '/login',
        templateUrl: 'views/login_login.html',
        controller: 'LoginLoginCtrl',
        title: 'Please Sign In'

      })
      .state('login.forget_password', {
        url: '/forget_password',
        templateUrl: 'views/forget_password.html',
        controller: 'forgetpasswordCtrl'

      })

      .state('login.reset_password', {
        url: '/reset_password/:id/:cid',
        templateUrl: 'views/reset_password.html',
        controller: 'resetpasswordCtrl'

      })
      .state('main', {
        url: "/main",
        abstract: true,
        templateUrl: "views/main_base.html",
        controller: "MainBaseCtrl"
      })
      .state('main.home', {
        url: "/home",
        templateUrl: "views/home.html",
        controller: "HomeCtrl",
        title: 'Points and Perfomance Dashboard'

        //                data: {
        //                "header_label" : "Points and Perfomance Dashboard"
        //           }
      })

      .state('main.company', {
        url: "/company",
        templateUrl: "views/company.html",
        controller: "CompanyCtrl",
        title: 'Company'
      })

      .state('main.distributor', {
        url: "/distributor",
        templateUrl: "views/distributor.html",
        controller: "DistributorCtrl",
        title: 'Distributor'
      })

      .state('main.terms_condition', {
        url: "/terms_condition",
        templateUrl: "views/terms_condition.html",
        controller: "LoginLoginCtrl"
      })

      .state('main.claim', {
        url: '/claim',
        templateUrl: 'views/claim.html',
        controller: 'ClaimCtrl',
        title: 'Invoice Upload'
        //            data: {
        //                "header_label" : "Invoice Upload"
        //            }
      })
      .state('main.claim_upload', {
        url: '/claim_upload',
        templateUrl: 'views/claim_upload.html',
        controller: 'ClaimUploadCtrl',
        title: 'Invoice Upload'
        //            data: {
        //                "header_label" : "Invoice Upload"
        //            }
      })

      .state('main.payment_upload', {
        url: '/payment_upload',
        templateUrl: 'views/payment_upload.html',
        controller: 'PaymentUploadCtrl',
        title: 'Payment Upload'

      })

      .state('main.payment', {
        url: '/payment',
        templateUrl: 'views/payment.html',
        controller: 'PaymentCtrl',
        title: 'Payment Upload'

      })

      .state('main.prog_admin', {
        url: '/prog_admin',
        templateUrl: 'views/program_management.html',
        controller: 'programCtrl',
        title: 'Program Management'
        //            data: {
        //                "header_label" : "Claim Entry"
        //            }
      })


      .state('main.upld_pos', {
        url: '/upld_pos',
        templateUrl: 'views/upld_pos.html',
        controller: 'programCtrl',
        title: 'Program Management'
        //            data: {
        //                "header_label" : "Claim Entry"
        //            }
      })




      .state('main.prog_admin_status', {
        url: '/prog_admin/:status',
        templateUrl: 'views/program_management.html',
        controller: 'programCtrl',
        title: 'Program Management'
      })


      .state('main.program', {
        url: '/program',
        templateUrl: 'views/program.html',
        controller: 'MainBaseCtrl',
        title: 'Program'
        //            data: {
        //                "header_label" : "Seep Program"
        //            }
      })

      .state('main.redemption', {
        url: '/redemption',
        templateUrl: 'views/rewards.html',
        controller: 'RedemptionCtrl',
        title: 'Order details'
        //            data: {
        //                "header_label" : "Order details"
        //            }
      })

      .state('main.redemption_detail', {
        url: '/redemption_detail/:order_id/:status/:order_date/:points',
        templateUrl: 'views/redemption_more.html',
        controller: 'RedemptiondetailCtrl',
        title: 'Order details'
        //            data: {
        //                "header_label" : "Claim Entry"
        //            }
      })

      .state('main.special', {
        url: '/special',
        templateUrl: 'views/special.html',
        controller: 'SeepCtrl',
        title: 'Seep Special'
        //            data: {
        //                "header_label" : "Seep Special"
        //            }
      })

      .state('main.profile', {
        url: '/profile',
        templateUrl: 'views/profile.html',
        controller: 'myprofileCtrl',
        title: 'My Profile'
        //            data: {
        //                "header_label" : "My Profile"
        //            }
      })

      .state('main.my_claim', {
        url: '/my_claim',
        templateUrl: 'views/myclaim.html',
        controller: 'myclaimCtrl',
        title: 'My Claim'
        //            data: {
        //                "header_label" : "Claim Entry"
        //            }
      })

      .state('main.program_details', {
        url: '/program_details/:claim_id/:user_id',
        templateUrl: 'views/program_details.html',
        controller: 'programdetailCtrl',
        title: 'Program Details'
        //                data: {
        //                    "header_label" : "Program Details"
        //                }
      })

      .state('main.changepass', {
        url: '/changepass',
        templateUrl: 'views/changepass.html',
        controller: 'changepassCtrl'
        //            data: {
        //                "header_label" : "Claim Entry"
        //            }
      })
  });

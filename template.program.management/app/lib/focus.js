/**
 * Created by DRIPL_1 on 05/01/2015.
 */

angular.module('vstford', [])
    .directive('focus', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                element.bind("keydown", function(event) {
                    if(event.which == 9) {
                        event.preventDefault();
                        var srcEle = element[0].id;
                        var currId = srcEle.slice(0, srcEle.lastIndexOf("-"));
                        var refChar = srcEle.lastIndexOf('-');
                        var index = srcEle.substring(refChar + 1);
                        index = parseInt(index);
                        var nextIndex = index + 1;
                        nextIndex = nextIndex.toString();
                        var nextId = '#' + currId + '-' + nextIndex;
                        var nextEle = angular.element(document.querySelector(nextId));
                        if(nextEle[0]) {
                            nextEle[0].focus();
                        }
                        else {
                            var nextId = '#' + currId + '-1';
                            var nextEle =      angular.element(document.querySelector(nextId));
                            nextEle[0].focus();
                        }
                    }
                })
            }
        }
    });
